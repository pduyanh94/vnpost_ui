module.exports = () => ({
  test: /\.(woff(2)?|ttf|eot|otf)$/,
  use: [
    {
      loader: 'file-loader',
      options: {
        name: '[name].[ext]',
        outputPath: 'fonts/',
      },
    },
  ],
});
