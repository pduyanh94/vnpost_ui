const rules = require('./rules');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const plugins = require('../webpack/plugins');
const PATHS = require('./paths');
const Dotenv = require('dotenv-webpack');
const webpack = require('webpack');


console.log(`Running webpack in ${process.env.NODE_ENV}`);

module.exports = () => {
  const isProduction =
    process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging';
  const hotMiddlewareScript = 'webpack-hot-middleware/client?reload=true';

  const devConfig = {
    mode: 'development',
    entry: [hotMiddlewareScript, PATHS.app],
    output: {
      path: PATHS.build,
      filename: 'bundle.js',
      publicPath: PATHS.public,
    },
    module: { rules: rules() },
    plugins: plugins({ production: false }),
    optimization: {
      minimizer: [new UglifyJsPlugin()],
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.css$/,
            chunks: 'all',
            enforce: true,
          },
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendor',
            chunks: 'all',
          },
        },
      },
    },
  };

  const prodConfig = {
    mode: 'production',
    entry: PATHS.app,
    output: {
      path: PATHS.build,
      filename: 'bundle.js',
      chunkFilename: '[name].js',
    },
    module: { rules: rules() },
    plugins: plugins({ production: true }),
    optimization: {
      minimizer: [new UglifyJsPlugin()],
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.css$/,
            chunks: 'all',
            enforce: true,
          },
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendor',
            chunks: 'all',
          },
        },
      },
    },
  };
  const configuration = isProduction ? prodConfig : devConfig;

  return configuration;
};
