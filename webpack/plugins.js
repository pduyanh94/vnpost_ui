const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const path = require('path');
const dirName = __dirname;
let arrDir = dirName.split(`/\/`);
arrDir[arrDir.length - 1] = '.env'
let newDir = arrDir.join('/\/')
const envPath = path.join(newDir)


module.exports = ({ production = false }) => {
  if (production) {
    return [
      new MiniCssExtractPlugin({
        filename: '[name].css',
      }),
           
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'process.env.API_HOST': JSON.stringify(process.env.API_HOST),
      }),
      new HtmlWebpackPlugin({
        template: 'client/index.html',
      }),
    ];
  } else {
    return [
      new MiniCssExtractPlugin({
        filename: '[name].css',
      }),
      new Dotenv({
        path: envPath
      }), 
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'process.env.API_HOST': JSON.stringify(process.env.API_HOST),
      }),
      new HtmlWebpackPlugin({
        template: 'client/index.html',
      }),
      new webpack.HotModuleReplacementPlugin(),
    ];
  }
};
