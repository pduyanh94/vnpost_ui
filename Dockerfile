FROM node:14-alpine

RUN mkdir /app && chown -R node:node /app \
 && apk update \
 && apk add --no-cache \
 build-base \
 python

WORKDIR /app

USER node

COPY --chown=node:node package.json ./

RUN rm -rf node_modules/ \
 && npm cache clean -f \
 && npm i \
 && npm audit fix

COPY --chown=node:node . .

EXPOSE 3000

ENV env_variable='dev'

RUN ["chmod", "+x", "/app/Entrypoint.sh"]

ENTRYPOINT ["sh", "/app/Entrypoint.sh"]