export const BACKEND_ENDPOINT_DEV = 'http://10.35.104.142:3000/api/v1';

export const USER_NAME_REGEX = /^[a-zA-Z0-9_.-]{1,50}$/;
export const EMAIL_REGEX = /^[\w-\.]+@([\w-]+\.)+([a-z|A-Z]{2,4}|(fsoft))$/;
export const FIELD_REQUIRED = 'Mandatory field is required.';
export const MAX_MIN_PHONE = /^[0-9]{11}$/;
export const LINK_REGEX =
  /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;

export const ACCOUNT = process.env.ACCOUNT;
export const SAS = process.env.SAS;
export const CONTAINER_NAME = process.env.CONTAINER_NAME;
