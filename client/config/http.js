import axios from 'axios';
import { BACKEND_ENDPOINT_DEV } from '../constant/constant';

export const handleResponse = (data) => data.data;

// const instance = axios.config({
//   baseURL: BACKEND_ENDPOINT_DEV,
//   timeout: 5000,
//   headers: { 'Content-Type': 'application/json' },
// });

export default axios;
