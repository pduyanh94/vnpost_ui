import axios from 'axios';

const API_URL = '/api/v1';

const getConfig = (param) => {
  if (param !== undefined) {
    const config = {
      params: param,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    };
    return { config };
  } else {
    const config = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    };
    return { config };
  }
};

export const webApiGet = (url, param = undefined) => {
  const config = getConfig(param);
  return {
    request: axios.get(`${API_URL}${url}`, config.config),
  };
};

export const webApiPost = (url, options) => {
  const config = getConfig();
  return {
    request: axios.post(`${API_URL}${url}`, options, config.config),
  };
};

export const webApiDeleted = (url, param = undefined) => {
  const config = getConfig();
  return {
    request: axios.delete(`${API_URL}${url}`, config.config),
  };
};

export const webApiPatch = (url, options) => {
  const config = getConfig();
  return {
    request: axios.patch(`${API_URL}${url}`, options, config.config),
  };
};

export const webApiUpdate = (url, options) => {
  const config = getConfig();
  return {
    request: axios.put(`${API_URL}${url}`, options, config.config),
  };
};
export let source = axios.CancelToken.source();
