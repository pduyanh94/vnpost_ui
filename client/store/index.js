import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducer';
import { composeWithDevTools as compose } from 'redux-devtools-extension';
import middlewares from '../middleware';

const configureStore = (initialState = {}) => {
  // const {loadCreateDocument, successCreateDocument, errorStatusCreateDocument, errorCreateDocument} = useSelector(state => state.createDocument)

  const store =  createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(...middlewares)),
  );
  store.subscribe(()=> {
    const statusUpload = store.getState().createDocument
   
  })

  return store
};

export default configureStore;
