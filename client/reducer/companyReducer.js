import {
  SAVE_COMPANY_DETAILS,
  SAVE_UPDATE_STATUS,
  RESET_COMPANY_DETAILS,
  SAVE_COMPANY_LIST,
  CLEAR_DATA_ON_LOGOUT,
  DASHBOARD_SUCCESS_MSG,
} from '../actions/actionTypes';

const initialState = {
  companyDetails: {},
  updateStatus: false,
  companyList: [],
  dashboardSuccessMsg: '',
};

const saveCompanyDetails = (state, { data }) => ({
  ...state,
  companyDetails: data,
});

const saveCompanyList = (state, { data }) => ({
  ...state,
  companyList: data.posts,
});

const saveDashboardSuccessMsg = (state, action) => ({
  ...state,
  dashboardSuccessMsg: action.data || '',
});

const saveUpdateStatus = (state, { data: { dataUpdated, dashboardSuccessMsg } }) => ({
  ...state,
  updateStatus: dataUpdated,
  dashboardSuccessMsg,
});

const resetCompanyDetails = (state) => ({
  ...state,
  companyDetails: {},
  updateStatus: false,
});

const clearData = () => ({ ...initialState });

const companyReducer = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_COMPANY_DETAILS:
      return saveCompanyDetails(state, action);
    case SAVE_UPDATE_STATUS:
      return saveUpdateStatus(state, action);
    case RESET_COMPANY_DETAILS:
      return resetCompanyDetails(state);
    case SAVE_COMPANY_LIST:
      return saveCompanyList(state, action);
    case CLEAR_DATA_ON_LOGOUT:
      return clearData();
    case DASHBOARD_SUCCESS_MSG:
      return saveDashboardSuccessMsg(state, action);

    default:
      return state;
  }
};

export default companyReducer;
