import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';

import './style.less';

const commonLoader = ({ size, color, type }) => {
  const extraClass = type === 'dashboard' ? 'dashboard-loader-container' : '';
  return (
    <div className={`default-values ${extraClass}`}>
      <CircularProgress disableShrink size={size} style={{ color }} />
    </div>
  );
};

commonLoader.defaultProps = {
  size: 24,
  color: '#fff',
  type: '',
};

commonLoader.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string,
  type: PropTypes.string,
};

export default commonLoader;
