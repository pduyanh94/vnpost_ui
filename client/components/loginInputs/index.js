import React from 'react';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';

import { upperCaseFirstLetter } from '../../utils';
import { Button } from '@material-ui/core';

const loginInputs = ({
  error,
  inputLabel,
  handleChange,
  value,
  type,
  iconType,
  iconAriaLabel,
  handleIconClick,
  handleIconMouseEvent,
  outlinedInputId,
}) => {
  return (
    <Grid item lg={12} md={12} sm={12} xs={12}>
      <FormControl variant="outlined" className="full-width" error={error}>
        <InputLabel htmlFor={outlinedInputId}>{inputLabel}</InputLabel>
        <OutlinedInput
          fullWidth
          required
          id={outlinedInputId}
          label={upperCaseFirstLetter(outlinedInputId)}
          name={outlinedInputId}
          onChange={handleChange}
          value={value}
          type={type}
          endAdornment={
            <InputAdornment position="end">
              <Button
                aria-label={iconAriaLabel}
                onClick={handleIconClick}
                onMouseDown={handleIconMouseEvent}
                edge="end"
              >
                {iconType}
              </Button>
            </InputAdornment>
          }
        />
      </FormControl>
    </Grid>
  );
};

loginInputs.defaultProps = {
  iconAriaLabel: '',
  handleIconClick: () => {},
  handleIconMouseEvent: () => {},
};

loginInputs.propTypes = {
  iconAriaLabel: PropTypes.string,
  handleIconClick: PropTypes.func,
  handleIconMouseEvent: PropTypes.func,
  error: PropTypes.bool.isRequired,
  type: PropTypes.string.isRequired,
  outlinedInputId: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  inputLabel: PropTypes.string.isRequired,
  iconType: PropTypes.object.isRequired,
};

export default loginInputs;
