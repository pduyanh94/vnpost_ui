import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress';
export const Loading = () => {
    return (
        <div>
            <CircularProgress disableShrink style={{display: 'block', margin: 'auto', width: '50px', height: '50px'}}/>
        </div>
    )
}
