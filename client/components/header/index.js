import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { connect } from 'react-redux';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import { bindActionCreators } from 'redux';
import { NavLink, withRouter } from 'react-router-dom';

import { clearDataOnLogout } from '../../actions/common_action';
import AdqLogo from '../../assets/images/vnpost.png';
import './style.less';
import moment from 'moment';

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // receiverId: JSON.parse(localStorage.getItem('user')).userId,
      anchorEl: null,
      open: false,
      countNotification: 0,
      options: [],
      // role: JSON.parse(localStorage.getItem('user')).role,
      isEndNoti: false,
    };

  }


  componentDidMount() {

  }

  render() {
    const { pathname } = this.props.location;

    return (
      <Grid container className="header" style={{ flexWrap: 'nowrap' }}>
        <Grid item md={1} sm={1} lg={1} xl={1} className="logo_container">
          <img
            src={AdqLogo}
            alt="Logo"
            onClick={() => {
              this.props.history.push('/');
            }}
          />
        </Grid>

        <Grid item md={9} sm={9} lg={9} xl={9} className="btn-menu">
          <NavLink to="/" className={pathname === '/' ? 'link-styled link-active' : 'link-styled'}>
            Dashboard
          </NavLink>
        </Grid>


      </Grid>
    );
  }
}

const mapStateToProps = (state) => ({
  // appToken: state.userReducer.appToken,
  // showLoader: state.commonReducer.showLoader,
});

const mapDispatchToProps = (dispatch) => ({
  clearDataOnLogout: bindActionCreators(clearDataOnLogout, dispatch),
  getUserInfo: () => dispatch(getUserInfo()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Index));

function calDurationTime(time) {
  let currentTime = moment(new Date(), 'YYYY-MM-DDTHH:mm');
  let startTime = moment(time, 'YYYY-MM-DDTHH:mm');
  let minute = moment.duration(currentTime.diff(startTime)).asMinutes();
  let duration = '';
  if (minute == 0) {
    duration = 'now';
  } else if (minute > 0 && minute < 60) {
    duration = 'last ' + Math.round(minute) + ' minute(s)';
  } else {
    duration = moment(time).format('DD-MMMM-YYYY') + ' at ' + moment(time).format('HH:mm');
  }
  return duration;
}
