import React from 'react';
import { Grid } from '@material-ui/core';
import './styles.less';

const PageHeader = ({ title, children, ...props }) => {
  return (
    <Grid item sm={12} className="w-100">
      <div className="title_container">
        <div className="title_content display_sm_none">
          <p>{title}</p>
          <h3>{title}</h3>
        </div>
        <div className="add_button_container">{children}</div>
      </div>
    </Grid>
  );
};

export default PageHeader;
