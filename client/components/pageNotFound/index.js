import React from 'react';
import PageNotFoundImage from '../../assets/images/ic_404.png';

import './style.less';

const pageNotFound = () => {
  return (
    <div className="pageNotFoundContainer">
      <img src={PageNotFoundImage} className="notFoundImage" />
      <h3 className="notFoundMessage">
        Oops, The Page you were looking for doesn’t exist
      </h3>
    </div>
  );
};

export default pageNotFound;
