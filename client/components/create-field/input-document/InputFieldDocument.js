import { TextField } from '@material-ui/core';
import React from 'react';
import './style.less';

export const InputFieldDocument = ({ field, form, label, isMultiline, isRequired }) => {
  const { name } = field;
  const { errors, touched } = form;
  const showErrors = errors[name] && touched[name];

  const handleKeyDown = (event, name) => {
    if (name === 'name') {
      if (field.value && field.value.length === 50) {
        event.preventDefault();
      }
    }
    if (name === 'link') {
      if (field.value && field.value.length === 255) {
        event.preventDefault();
      }
    }
  };

  const handleOnPaste = (event) => {
    if (name === 'name') {
      const valueOfPaste = (event.clipboardData || window.clipboardData).getData('text');
      if (valueOfPaste.length > 50) {
        const subValueOfPaste = valueOfPaste.substring(0, 50);
        form.setFieldValue(name, subValueOfPaste);
        event.preventDefault();
      }
    }
  };

  return (
    <div className="form-group">
      <label className="label">
        {label}
        {isRequired ? <span className="required"> *</span> : null}
      </label>
      <TextField
        id={name}
        className="input_field_document"
        autoComplete="off"
        autoFocus={name === 'name'}
        variant="outlined"
        {...field}
        onKeyPress={(event) => handleKeyDown(event, name)}
        onPaste={(event) => handleOnPaste(event)}
        error={showErrors}
        multiline={isMultiline}
        rowsMax={4}
      />

      {showErrors && <i className="errors">{errors[name]}</i>}
    </div>
  );
};
