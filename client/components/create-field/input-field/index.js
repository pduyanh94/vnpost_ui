import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './style.less';
import TextField from '@material-ui/core/TextField';
import { PHONE_NUMBER_REGEX, USER_NAME_REGEX } from '../../../constant/constant';

InputField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  type: PropTypes.string,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  inputProps: PropTypes.object,
};

InputField.defaultProps = {
  type: 'text',
  label: '',
  disabled: false,
  inputProps: { maxLength: 100 },
};

const contactTypeField = ['phone', 'ceoContact', 'chairmanContact'];

function InputField(props) {
  const { field, form, label, inputProps, disabled } = props;

  const { name } = field;
  const { errors, touched } = form;
  const showErrors = errors[name] && touched[name];

  const handleKeyDown = (event, name) => {
    if (name === 'firstname' || name === 'lastname') {
      if (field?.value.length === 100 || event?.target?.value.length === 100) {
        event.preventDefault();
      }
    }
    if (name === 'assetName' || name === 'clusterName') {
      if ((field.value && field.value.length === 50) || (event.target.value && event.target.value.length === 50)) {
        event.preventDefault();
      }
    }
  };

  const onChange = (e) => {
    const {
      target: { value },
    } = e;
    if (contactTypeField.indexOf(name) !== -1) {
      if (!value) {
        field.onChange({
          target: {
            name: name,
            value: valueTemp,
          },
        });
      }
      let valueTemp = value;
      const valueTrim = value.replace(' ', '');
      if (valueTrim.length > 15) {
        return;
      }
      if (valueTrim.length > 3 && value[3] !== ' ') {
        valueTemp = value.slice(0, 3) + ' ' + value.slice(3);
      }
      if (/^[0-9 ]*$/.test(valueTemp)) {
        field.onChange({
          target: {
            name: name,
            value: valueTemp,
          },
        });
      }
    } else {
      let valueTemp = field?.value;
      switch (name) {
        case 'firstname':
        case 'lastname':
          if (/^([^0-9]*)$/.test(value)) {
            valueTemp = value;
          }
          break;
        case 'shareHolding':
          if (/^([0-9])*\.?(([0-9]){0,2})$/.test(value) && Number(value) <= 100) {
            valueTemp = value;
          }

          break;
        default: {
          valueTemp = value;
        }
      }
      field.onChange({
        target: {
          name: name,
          value: valueTemp,
        },
      });
    }
  };

  const handleOnPaste = (event) => {
    if (name === 'phone' || name === 'ceoContact' || name === 'chairmanContact') {
      const valueOfPaste = (event.clipboardData || window.clipboardData).getData('text');
      if (!/^[0-9]*$/.test(valueOfPaste)) {
        event.preventDefault();
      }
    }
    if (name === 'username' || name === 'clusterName' || name === 'assetName') {
      const valueOfPaste = (event.clipboardData || window.clipboardData).getData('text');
      const totalValue = valueOfPaste + field.value;

      let subValueOfPaste;
      if (valueOfPaste.length > 50) {
        subValueOfPaste = valueOfPaste.substring(0, 50);
        form.setFieldValue(name, subValueOfPaste);
        event.preventDefault();
      }
      if (totalValue && totalValue.length > 50) {
        subValueOfPaste = totalValue.substring(0, 50);
        form.setFieldValue(name, subValueOfPaste);
        event.preventDefault();
      }
    }

    if (name === 'firstname' || name === 'lastname') {
      const valueOfPaste = (event.clipboardData || window.clipboardData).getData('text');
      const totalValue = valueOfPaste + field.value;
      if (valueOfPaste.length > 100) {
        const subValueOfPaste = valueOfPaste.substring(0, 100);
        form.setFieldValue(name, subValueOfPaste);
        event.preventDefault();
      }
      if (totalValue && totalValue.length > 100) {
        subValueOfPaste = totalValue.substring(0, 100);
        form.setFieldValue(name, subValueOfPaste);
        event.preventDefault();
      }
    }
  };

  return (
    <div className="form-group">
      <label className="label">
        {label}
        <span className="required"> *</span>
      </label>
      <TextField
        autoComplete="off"
        id={name}
        autoFocus={name === 'username' || name === 'assetName' || name === 'clusterName'}
        variant="outlined"
        multiline={name === 'description'}
        rowsMax={4}
        InputProps={contactTypeField.indexOf(name) !== -1 ? { startAdornment: <span>+</span> } : inputProps}
        {...field}
        onKeyPress={(event) => handleKeyDown(event, name)}
        onPaste={(event) => handleOnPaste(event)}
        onChange={onChange}
        disabled={disabled}
        error={showErrors}
      />
      {showErrors && <i className="errors">{errors[name]}</i>}
    </div>
  );
}

export default InputField;
