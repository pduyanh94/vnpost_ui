import { FormControlLabel, Radio, RadioGroup } from '@material-ui/core';
import React from 'react';
import './style.less'
export const RadioOptionCustom = ({ label, field, form, handleChangeOption }) => {
  const { name, value } = field;
  const { errors, touched } = form;
  const showErrors = errors[name] && touched[name];


  const handleChangeOptionRadio = (event) => {

    handleChangeOption(event.target.value)
    form.setFieldValue('chooseOption', event.target.value)
  }
  return (
    <div className="form-group">
      <label className="label">
        {label}
        <span className="required"> *</span>
      </label>
      <RadioGroup
        value={field.value}
        onChange={handleChangeOptionRadio}
        className="option-group"
      >
        <FormControlLabel
          value="file"
          control={<Radio color="primary"/>}
          color="primary"
          label="Upload"
        />
        <FormControlLabel
          value="link"
          control={<Radio color="primary"/>}
          color="primary"
          label="Link"
        />
      </RadioGroup>
      {showErrors && <i className="errors-document">{errors[name]}</i>}

    </div>
  );
};
