import { MenuItem, Select } from '@material-ui/core';
import React from 'react';
import { BootstrapInput } from '../../../componentConfig/StyledSelection';
import './style.less';

export const CustomSelectField = ({ options, field, form, label, isRequired }) => {
  const { name, value } = field;
  const { errors, touched } = form;
  const showErrors = errors[name] && touched[name];

  const handleChangeOption = (event) => {
    const changeEvent = {
      target: {
        name: name,
        value: event.target.value,
      },
    };
    field.onChange(changeEvent);
  };
  return (
    <div className="form-group">
      <label className="label">
        {label}
        {isRequired ? <span className="required"> *</span> : null}
      </label>

      <Select
        labelId="customized-select-label"
        id="customized-select"
        className="customized-select-document"
        variant="filled"
        style={{ marginTop: '0px' }}
        {...field}
        value={value}
        defaultValue={value}
        onChange={handleChangeOption}
        input={<BootstrapInput />}
      >
        {options.map((option, index) => (
          <MenuItem value={option} className="option-selected" key={index}>
            {option}
          </MenuItem>
        ))}
      </Select>
      {showErrors && <i className="errors">{errors[name]}</i>}
    </div>
  );
};
