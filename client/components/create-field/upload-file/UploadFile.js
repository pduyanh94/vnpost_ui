import { TextField } from '@material-ui/core';
import React, { useState } from 'react';
import './style.less';

export const UploadFile = ({
  field,
  form,
  type,
  label,
  inputProps,
  disabled,
  isMultiline,
  handleChangeUpdate,
}) => {
  const { name, value } = field;
  const { errors, touched } = form;
  const showErrors = errors[name] && touched[name];
 

  const [fileBase64, setFileBase64] = useState({
    values: '',
    name: '',
  });
  const [nameFile, setNameFile] = useState('')
  const getBase64 = (file) => {
    return new Promise((resolve) => {
      let fileInfo;
      let baseURL = '';
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        baseURL = reader.result;
        resolve(baseURL);
      };
     
    });
  };



  const handleFileInputChange = (event) => {
    const file = event.target.files[0];
 
    getBase64(file)
      .then((result) => {
       
        file['base64'] = result;
       
        setFileBase64({
          ...fileBase64,
          values: result,
          name: file['name'],
        });
        handleChangeUpdate({
          values: result,
          name: file['name'],
          files: file
        });
        form.setFieldValue('file', 
        {
          values: result,
          name: file['name'],
        }
      )
      
      })
      .catch((err) => {
      
      });

    setFileBase64({
      ...fileBase64,
      values: event.target.files[0],
      name: event.target.files[0].name,
    });
  };

  return (
    <div className="form-group">
      <label className="label">
        {label}
        <span className="required"> *</span>
      </label>

      <div className="form-control-upload">
        <TextField
          id="upload"
          variant="outlined"
          error={showErrors}
          value={fileBase64.name}
          disabled={fileBase64.name === ''}
          className="not-allowed-edit-upload"
         
        />
        <label className="custom-file-upload">
          <input
            type="file"
           
            onChange={(event) => {
              handleFileInputChange(event);
            }}
            title=""
          />
          Browse
        </label>
      </div>
      {showErrors && <i className="errors">{errors[name]}</i>}
    </div>
  );
};
