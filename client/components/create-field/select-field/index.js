import React, { useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import './style.less';

SelectField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  options: PropTypes.array,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
  isClearable: PropTypes.bool,
  isMulti: PropTypes.bool,
};

SelectField.defaultProps = {
  array: [],
  label: '',
  disabled: false,
  placeholder: '',
  isClearable: true,
  isMulti: false,
};

const customStyles = {
  control: (provided, state) => ({
    ...provided,
    minHeight: '40px',
    cursor: 'pointer',
    borderRadius: 8,
  }),
  option: (provided, state) => {
    return {
      ...provided,
      cursor: 'pointer',
    };
  },
  container: (provided, state) => ({
    ...provided,
    cursor: 'pointer',
  }),
  menu: (provided, state) => ({
    ...provided,
    zIndex: '9999999',
  }),
  menuList: (provided, state) => ({
    ...provided,
    height: 'auto',
    cursor: 'pointer',
    overflowY: 'scroll',
    zIndex: '999999',
  }),
  menuPortal: (provided, state) => ({
    ...provided,
    zIndex: '99999',
  }),
};

function SelectField(props) {
  const {
    field,
    form,
    options = [],
    label,
    placeholder,
    disabled,
    isClearable,
    isMulti,
    isRequired,
    onSetValueAssetId,
    defaultValue,
    onDisable,
  } = props;

  const { name, value } = field;
  const { errors, touched } = form;
  const showErrors = errors[name] && touched[name];
  const [selectedOption, setSelectedOption] = useState([]);

  useEffect(() => {
    if (value) {
      const defaultValueTemp = options.find((option) => {
        return option.value === value;
      });
      setSelectedOption(defaultValueTemp);
      return;
    }

    if (defaultValue) {
      setSelectedOption(defaultValue);
    }

    return () => {
      onDisable && onDisable();
    };
  }, []);

  useEffect(() => {
    let selectedValue = '';
    if (!selectedOption) {
      selectedValue = '';
    } else if (selectedOption['value']) {
      selectedValue = selectedOption.value;
    } else {
      selectedValue = selectedOption.map((item) => item.value).join(',');
    }

    field.onChange({
      target: {
        name: name,
        value: selectedValue,
      },
    });
  }, [selectedOption]);

  return (
    <div className="form-group">
      <label className="label">
        {label} {isRequired ? <span className="required">*</span> : null}
      </label>

      <Select
        id={name}
        {...field}
        value={selectedOption}
        onChange={(data) => setSelectedOption(data)}
        placeholder={placeholder}
        isDisabled={disabled}
        options={options}
        isClearable={isClearable}
        className={showErrors && 'invalid-select'}
        isMulti={isMulti}
        openMenuOnFocus
        styles={customStyles}
      />

      {showErrors && <i className="errors">{errors[name]}</i>}
    </div>
  );
}

export default SelectField;
