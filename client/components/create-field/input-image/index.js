import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './style.less';
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';

InputImage.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  type: PropTypes.string,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  inputProps: PropTypes.object,
  imageUrl: PropTypes.string,
  //handleChangeImage: PropTypes.func,
};

InputImage.defaultProps = {
  type: 'text',
  label: '',
  disabled: false,
};

function InputImage(props) {
  const { name } = props.field;
  const { errors, touched } = props.form;
  const showErrors = errors[name] && touched[name];
  const [imageUrl, setImageUrl] = useState('');
  const [uploadChecked, setUploadChecked] = useState(false);
  const bufferImage = props.field && props.field.value && typeof props.field.value == 'string' && props.field.value;

  const handleOnChangeImage = (e) => {
    let image = e.target.files[0];
    setUploadChecked(true);
    let fileType = image['type'];
    const validImageTypes = ['image/jpeg', 'image/png'];
    if (validImageTypes.includes(fileType)) {
      props.handleChangeImage(image);
      setImageUrl(URL.createObjectURL(image));
      props.form.setFieldValue(props.field.name, e.target.files);
    } else {
      props.form.setFieldError(name, 'Invalid file!');
      props.handleChangeImage(null);
      setImageUrl('');
      props.form.setFieldValue(props.field.name, e.target.files);
    }
  };

  return (
    <div className="form-group">
      <label className="label">
        {props.label}
        <span className="required"> *</span>
      </label>
      <div className="file-area">
        <input type="file" onChange={handleOnChangeImage} accept="image/*" />
        <div className="file-dummy">
          {bufferImage || imageUrl ? (
            <img src={bufferImage || imageUrl} alt="" className="input-image" />
          ) : (
            <AddIcon style={{ color: '#B2B2B2' }} className="add-icon" />
          )}
        </div>
      </div>
      {showErrors && <i className="errors">{errors[name]}</i>}
    </div>
  );
}

export default InputImage;

function toBase64(arr) {
  //arr = new Uint8Array(arr) if it's an ArrayBuffer
  return btoa(arr.reduce((data, byte) => data + String.fromCharCode(byte), ''));
}
