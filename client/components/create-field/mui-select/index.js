import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import './style.less';
import { BootstrapInput } from '../../../componentConfig/StyledSelection';

const MuiSelect = ({ data, onChange, multiple, value, className, ...props }) => {
  return (
    <Select
      onChange={onChange}
      multiple={multiple}
      value={value || ''}
      className={`select_container ${className}`}
      rowsMax={20}
      input={<BootstrapInput />}
      {...props}
    >
      {data.length > 0 ? (
        data.map((item, index) => {
          return (
            <MenuItem key={`select_item_${item.value}_${index}`} value={item.value} className="select_item">
              {item.text}
            </MenuItem>
          );
        })
      ) : (
        <MenuItem value="">No options</MenuItem>
      )}
    </Select>
  );
};

export default MuiSelect;
