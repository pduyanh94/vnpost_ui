import React from 'react';
import PropTypes from 'prop-types';
import './style.less';
import TextField from '@material-ui/core/TextField';

TextArea.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  type: PropTypes.string,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  inputProps: PropTypes.object,
};

TextArea.defaultProps = {
  type: 'text',
  label: '',
  disabled: false,
  inputProps: { maxLength: 255 },
};

function TextArea(props) {
  const { field, form, type, label, inputProps, disabled } = props;

  const { name } = field;
  const { errors, touched } = form;
  const showErrors = errors[name] && touched[name];

  return (
    <div className="form-group">
      <label className="label">
        {label}
        <span className="required"> *</span>
      </label>
      <TextField
        className="text-area"
        multiline
        id={name}
        variant="outlined"
        {...field}
        type={type}
        disabled={disabled}
        error={showErrors}
        inputProps={inputProps}
        rows={4}
      />
      {showErrors && <i className="errors">{errors[name]}</i>}
    </div>
  );
}

export default TextArea;
