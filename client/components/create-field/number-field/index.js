import React from 'react';
import PropTypes from 'prop-types';
import './style.less';
import TextField from '@material-ui/core/TextField';
import NumberFormat from 'react-number-format';

NumberField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  label: PropTypes.string,
};

NumberField.defaultProps = {
  label: '',
};

function NumberField(props) {
  const { field, form, label } = props;

  const { name } = field;

  const { errors, touched } = form;
  const showErrors = errors[name] && touched[name];

  return (
    <div className="form-group">
      <label className="label">
        {label}
        <span className="required"> *</span>
      </label>
      <div>
        <span>+971</span>
        <NumberFormat
          className={showErrors ? 'invalid-number' : 'number-field'}
          id={name}
          {...field}
          isNumericString={true}
        />
      </div>

      {showErrors && <i className="errors">{errors[name]}</i>}
    </div>
  );
}

export default NumberField;
