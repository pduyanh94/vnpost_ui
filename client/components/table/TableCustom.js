import React from 'react';
import { Table, TableBody, TableCell, TableContainer, TableRow } from '@material-ui/core';
import TableHeadWithSorting from '../tableHeader';
import { TableRowCustom } from './TableRowCustom';

const filterCells = (field) => field.id !== 'action';

export const TableCustom = (props) => {
  const {
    rows,
    headCells = [],
    order = '',
    limit,
    currentPage,
    orderBy,
    onChangleRequestSort,
    roleScreen,
    onCreateUser,
    handleOpenAlertSuccess,
    onRefresh,
    renderActions,
    companyIsAsset,
    companyIsCluster,
  } = props;

  const handleRequestSort = (event, property) => {
    onChangleRequestSort(property);
  };

  const handleMessAlert = (value) => {
    handleOpenAlertSuccess(value);
  };

  return (
    <TableContainer className="table_custom_container">
      <Table aria-labelledby="tableTitle" size="medium" aria-label="enhanced table">
        <TableHeadWithSorting order={order} orderBy={orderBy} onRequestSort={handleRequestSort} headCells={headCells} />
        {rows.length > 0 ? (
          <TableBody>
            {rows.map((row, index) => {
              return (
                <TableRowCustom
                  className="table_row_custom"
                  row={row}
                  roleScreen={roleScreen}
                  onRefresh={onRefresh}
                  headCells={headCells.filter(filterCells)}
                  key={index}
                  stt={currentPage > 1 ? (currentPage - 1) * limit + index + 1 : index + 1}
                  renderActions={renderActions}
                  onCreateUser={onCreateUser}
                  handleOpenAlertSuccess={handleMessAlert}
                  companyIsAsset={companyIsAsset}
                  companyIsCluster={companyIsCluster}
                />
              );
            })}
          </TableBody>
        ) : (
          <TableBody>
            <TableRow>
              <TableCell colSpan={10} style={{ fontWeight: 600 }}>
                No data available matching search criteria.
              </TableCell>
            </TableRow>
          </TableBody>
        )}
      </Table>
    </TableContainer>
  );
};
