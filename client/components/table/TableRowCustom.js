import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { TableCell } from '@material-ui/core';
import DeletedIcon from '../../assets/icons/delete.svg';
import EditOutline from '../../assets/icons/edit-outline.svg';
import Disable from '../../assets/icons/disable.svg';
import Active from '../../assets/icons/active.svg';
import PropTypes from 'prop-types';

import { StyledTableRow } from '../../componentConfig/StyledTableCell';

import { monthMap } from '../../componentConfig';
import BootstrapTooltip from '../tooltip';
import './style.less';

const user = JSON.parse(localStorage.getItem('user'));
const role = user?.role;

const parseCellText = (key, typeRender, row, isTitle = false) => {
  const value = row[key];
  if (typeRender) {
    switch (typeRender) {
      case 'MONTH':
        return monthMap[value];
      case 'APPROVAL_STATUS':
        return isTitle ? (
          value
        ) : (
          <p className={`${value.toLowerCase().replace(/\s/g, '-')} text-center company_status`}>
            {value}
          </p>
        );
      case 'LOCAL_DATE':
        return new Date(value).toLocaleDateString();
      case 'LINK':
        return isTitle ? (
          'View dashboard'
        ) : (
          <Link className="table_cell_link" to={role !== 'DU' ? `/dashboard/${row.id}` : '/'}>
            View dashboard
          </Link>
        );
      case 'CAPITALIZE':
        return isTitle ? (
          <span className="text-capitalize">{value}</span>
        ) : (
          <p className="text-capitalize">{value}</p>
        );
      case 'ENTITY_TO_ASSET':
        return value === 'Entity' ? 'Asset' : value;
      default:
        return value || '';
    }
  }

  switch (key) {
    case 'phone':
    case 'ceoContact':
    case 'chairmanContact':
      return value ? `+${value}` : '';
    case 'role':
      switch (value) {
        case 'DU':
        case 'User':
          return 'Dashboard User';
        case 'Approver':
        case 'ID':
          return 'Approver';
        default:
          return value;
      }
    default:
      return value || '';
  }
};

export const TableRowCustom = (props) => {
  const {
    row,
    stt,
    renderActions,
    roleScreen,
    headCells,

  } = props;

  const [isActive, setIsActive] = useState(row.isActive);
  



  return (
    <>
      <StyledTableRow hover key={row.id}>
        {headCells.length > 0
          ? headCells.map((field, index) => {
              const { id, width, typeRender, render } = field;
              if (id === 'stt') {
                return (
                  <TableCell key={`cel-${stt}-${index}`} className="table_cell_custom">
                    {stt}
                  </TableCell>
                );
              }
              if (render) {
                return (
                  <TableCell key={`cel-${stt}-${index}`} style={{ width: width }}>
                    <div style={{ width: width }}>{render(row)}</div>
                  </TableCell>
                );
              }

              return (
                <TableCell
                  key={`cel-${stt}-${index}`}
                  id={`enhanced-table-checkbox-${index}`}
                  style={{ width: width }}
                >
                  <div style={{ width: width }}>
                    <BootstrapTooltip
                      title={parseCellText(id, typeRender, row, true) || ''}
                      placement="top-start"
                    >
                      <span className={`table_cell_custom ${typeRender}`}>
                        {parseCellText(id, typeRender, row)}
                      </span>
                    </BootstrapTooltip>
                  </div>
                </TableCell>
              );
            })
          : null}
        <TableCell>
          {roleScreen === 'users' && (
            <div className="cell-action">
              <div className="btn-action" onClick={handleEditUser}>
                <img src={EditOutline} />
              </div>
              <div
                className="btn-action"
                onClick={() =>
                  handleOpenConfirmDeleteUser(
                    `Are you sure you want to delete ${row.firstName} ${row.lastName} (${row.email})?`,
                  )
                }
              >
                <img src={DeletedIcon} />
              </div>
              <div className="btn-action" onClick={handleOpenConfirmActive}>
                <img src={`${isActive ? Disable : Active}`} />
              </div>
            </div>
          )}
          {roleScreen === 'asset' && (
            <div className="cell-action">
              <div className="btn-action" onClick={() => setOpenEditAsset(true)}>
                <img src={EditOutline} />
              </div>
              <div
                className="btn-action"
                onClick={() =>
                  handleOpenConfirmDeleteUser(`Are you sure you want to delete ${row.assetName}?`)
                }
              >
                <img src={DeletedIcon} />
              </div>
            </div>
          )}
          {roleScreen === 'cluster' && (
            <div className="cell-action">
              <div className="btn-action" onClick={handleEditUser}>
                <img src={EditOutline} />
              </div>
              <div
                className="btn-action"
                onClick={() => {
                  handleOpenConfirmDeleteUser(`Are you sure you want to delete ${row.name}?`);
                }}
              >
                <img src={DeletedIcon} />
              </div>
            </div>
          )}
          {renderActions && renderActions(row)}
        </TableCell>
      </StyledTableRow>

    </>
  );
};

TableRowCustom.proTypes = {
  row: PropTypes.object,
  stt: PropTypes.number,
  keyOfData: PropTypes.array,
  roleScreen: PropTypes.string.isRequired,
};
