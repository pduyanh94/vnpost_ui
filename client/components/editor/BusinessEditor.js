import React, { useEffect, useState } from 'react';
import { useQuill } from 'react-quilljs';
import ClearIcon from '@material-ui/icons/Clear';
import 'quill/dist/quill.snow.css';
import './style.less'

const CustomToolbar = () => (
  <div id="toolbar">
    <button className="ql-bold" />
    <button className="ql-italic"/>
    <button className="ql-underline"/>
    <button className="ql-strike"/>
    <select className="ql-font">
      <option value="arial" selected>
        Arial
      </option>
      <option value="comic-sans">Roboto</option>
    </select>

    <select className="ql-header"  value="1">
      <option value="1"></option>
      <option value="2"></option>
      <option value="3"></option>
      <option value="4"></option>
      <option value="5"></option>
      <option value="6"></option>
      <option selected></option>
    </select>
  
    <select className="ql-align" >
      <option selected="selected"></option>
      <option value="center"></option>
      <option value="right"></option>
      <option value="justify"></option>
    </select>
    <button className="ql-list" value="ordered" />
    <button className="ql-list" value="bullet" />
    <button className="ql-indent" value="-1" />
    <button className="ql-indent" value="+1" />

   

   
    <button className="ql-clean"></button>
   
  </div>
);


export const BusinessEditor = ({onUpdate, value}) => {
  const { quill, quillRef, Quill  } = useQuill({
      modules: {
          toolbar: '#toolbar',
          clipboard: {
            matchVisual: false
          }
          
      },
      formats: ["size", "bold", "italic", "underline", "strike", "font", "header", "list", "indent", "align","color","blockquote", 'clean']
  });

  
  useEffect(() => {
      if(quill){
          
          quill.on('text-change', ()=> {
            const text = quill.getText();
         
              const html = quill.root.innerHTML;
              onUpdate(html, text)
            
          })
          if(value){
            quill.root.innerHTML = value
          }
      }
     
  }, [quill])
  
  return (
    <div style={{ width: '100%', border: '1px solid #E1E1E1',  }} className="custom-editor">
      <CustomToolbar />
      <div id="editor" />
      <div ref={quillRef} />
    </div>
  );
};
