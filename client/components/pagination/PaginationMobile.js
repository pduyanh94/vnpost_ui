import React, { useState } from 'react';
import LastPageIcon from '@material-ui/icons/LastPage';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { Button, makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexWrap: 'nowrap',
    flexDirection: 'row',
    alignItems: 'center',
    minWidth: '0px',
    '.MuiButton-text': {
      padding: '0px',
    },
    '& .MuiButton-root': {
      minWidth: 'auto',
    },
  },
});

export const PaginationMobile = ({ count, page, onChange }) => {
  const classes = useStyles();
  const [newPage, setNewPage] = useState(page);

  const handleChangePage = (field) => {
    if (field === 'lastPage') {
      onChange(+count);
      setNewPage(count);
    }
    if (field === 'firstPage') {
      onChange(1);
      setNewPage(1);
    }
    if (field === 'backPage') {
      const backPage = newPage - 1;
      onChange(backPage);
      setNewPage(backPage);
    }
    if (field === 'forwardPage') {
      const forwardPage = newPage + 1;
      onChange(forwardPage);
      setNewPage(forwardPage);
    }
  };

  return (
    <div className={classes.root}>
      <Button disabled={page === 1} onClick={() => handleChangePage('firstPage')}>
        <FirstPageIcon />
      </Button>
      <Button disabled={page === 1} onClick={() => handleChangePage('backPage')}>
        <ArrowBackIosIcon fontSize="small" />
      </Button>
      <Button
        style={{
          backgroundColor: '#00205C',
          borderRadius: '4px',
          color: 'white',
          size: 'small',
          height: '24px',
          width: '30px !important',
        }}
      >
        {page}
      </Button>
      <Button disabled={page >= count} onClick={() => handleChangePage('forwardPage')}>
        <ArrowForwardIosIcon fontSize="small" />
      </Button>
      <Button disabled={page === count} onClick={() => handleChangePage('lastPage')}>
        <LastPageIcon />
      </Button>
    </div>
  );
};
