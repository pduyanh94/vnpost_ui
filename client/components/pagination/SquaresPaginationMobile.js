import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Pagination from '@material-ui/lab/Pagination';
import { InputBase, MenuItem, Select } from '@material-ui/core';
import { PaginationMobile } from './PaginationMobile';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    '& > *': {
      marginTop: theme.spacing(2),
    },
    '& .MuiPaginationItem-page.Mui-selected': {
      backgroundColor: '#00205C',
      color: 'white',
    },
    '& .MuiSelect-select.MuiSelect-select': {
      color: 'white',
    },
    '& .MuiSelect-select.MuiSelect-select:focus': {
      backgroundColor: '#00205C',
    },
  },
  perRow: {
    display: 'flex',
  },
  pagination: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: '50px',
  },
  colorStyle: {
    backgroundColor: '#00205C',
    color: 'white',
  },
  selectDisplay: {
    display: 'flex',
  },
  select: {
    paddingLeft: 8,
    paddingRight: 24,
    textAlign: 'right',
    textAlignLast: 'right',
    backgroundColor: '#00205C',
    borderRadius: 4,
    '& .MuiSelect-select:focus': {
      backgroundColor: '#00205C !important',
    },
  },
  selectIcon: {
    color: 'white',
  },
  input: {
    color: 'inherit',
    fontSize: 'inherit',
    flexShrink: 0,
  },
  menuItem: {},
  'Mui-selected': {},
}));

export const SquaresPaginationMobile = ({
  count,
  rowsPerPageOptions,
  rowsPerPage,
  page,
  onRowsPerPageChange,
  onPageChange,
}) => {
  const classes = useStyles();
  const handleChangeRowsPerPageChange = (event) => {
    onRowsPerPageChange(event.target.value);
  };
  const handleChangePage = (newPage) => {
    onPageChange(+newPage);
  };
  return (
    <div className={classes.root}>
      <div className={`${classes.pagination} pagination_mobile`}>
        <div className={classes.perRow}>
          <p>Row per page: </p>
          {rowsPerPageOptions && rowsPerPageOptions.length > 1 && (
            <Select
              classes={{
                select: classes.select,
                icon: classes.selectIcon,
              }}
              input={<InputBase className={clsx(classes.input, classes.selectRoot)} />}
              value={rowsPerPage}
              onChange={(event) => handleChangeRowsPerPageChange(event)}
            >
              {rowsPerPageOptions.map((rowsPerPageOption) => (
                <MenuItem className={classes.menuItem} key={rowsPerPageOption} value={rowsPerPageOption}>
                  {rowsPerPageOption}
                </MenuItem>
              ))}
            </Select>
          )}
        </div>
        <PaginationMobile count={count} page={page} onChange={handleChangePage} />
      </div>
    </div>
  );
};
