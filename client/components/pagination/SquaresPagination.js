import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Pagination from '@material-ui/lab/Pagination';
import { InputBase, MenuItem, Select } from '@material-ui/core';
import { PaginationMobile } from './PaginationMobile';
import ChervonWhiteIcon from '../../assets/icons/chervon_white.svg';
import './style.pagination.less';
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    '& .MuiPaginationItem-page.Mui-selected': {
      backgroundColor: '#00205C',
      color: 'white',
    },
    '& .MuiSelect-select.MuiSelect-select': {
      color: 'white',
    },
    '& .MuiSelect-select.MuiSelect-select:focus': {
      backgroundColor: '#00205C',
    },
  },
  perRow: {
    display: 'flex',
  },
  colorStyle: {
    backgroundColor: '#00205C',
    color: 'white',
  },
  selectDisplay: {
    display: 'flex',
  },
  selectRoot: {
    marginRight: 32,
    marginLeft: 8,
  },
  select: {
    paddingLeft: 8,
    borderRadius: 4,
    textAlign: 'right',
    textAlignLast: 'right',
    backgroundColor: '#00205C',
    '& .MuiSelect-select:focus': {
      backgroundColor: '#00205C !important',
    },
  },
  selectIcon: {
    color: 'white',
  },
  input: {
    color: 'inherit',
    fontSize: 'inherit',
    flexShrink: 0,
  },
  menuItem: {},
  'Mui-selected': {},
}));

export const SquaresPagination = ({
  count,
  rowsPerPageOptions,
  rowsPerPage,
  page,
  onRowsPerPageChange,
  onPageChange,
}) => {
  const classes = useStyles();
  const handleChangeRowsPerPageChange = (event) => {
    onRowsPerPageChange(event.target.value);
  };
  const handleChangePage = (event, newPage) => {
    onPageChange(+newPage);
  };
  return (
    <div className={`spares-container ${classes.root}`}>
      <div className={classes.perRow}>
        <p style={{ marginRight: '10px' }}>Row per page: </p>
        {rowsPerPageOptions && rowsPerPageOptions.length > 1 && (
          <Select
            classes={{
              select: classes.select,
              icon: classes.selectIcon,
            }}
            input={<InputBase className={clsx(classes.input, classes.selectRoot)} />}
            value={rowsPerPage}
            onChange={(event) => handleChangeRowsPerPageChange(event)}
            IconComponent={() => <img style={{ marginLeft: -18 }} src={ChervonWhiteIcon} />}
          >
            {rowsPerPageOptions.map((rowsPerPageOption) => (
              <MenuItem className={classes.menuItem} key={rowsPerPageOption} value={rowsPerPageOption}>
                {rowsPerPageOption}
              </MenuItem>
            ))}
          </Select>
        )}
      </div>
      <Pagination
        count={count}
        showFirstButton
        showLastButton
        shape="rounded"
        page={page}
        onChange={handleChangePage}
        className="styled-pagination"
      />
    </div>
  );
};
