import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import './style.less';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function AlertSuccess(props) {
  const { open, handleClose, message, isAlertError } = props;

  return (
    <Snackbar
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      open={open}
      autoHideDuration={3000}
      onClose={handleClose}
      className={`snackbar-container ${isAlertError && 'alert-error'}`}
    >
      <Alert className="content" onClose={handleClose} severity="success" className="alert-success-mobile">
        <p>Success Message</p>
        {message}
      </Alert>
    </Snackbar>
  );
}

export default AlertSuccess;
