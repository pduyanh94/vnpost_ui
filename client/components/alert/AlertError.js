import React, { useEffect } from 'react';
import { Fade, Snackbar } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import { useDispatch } from 'react-redux';
import { resetErrorTimeLogin } from '../../actions/user_action';
import { resetDocument } from '../../actions/actionCreater/actionCreaterDocument';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export const AlertError = (props) => {
  const { open, handleClose, message, checked } = props;
  const dispatch = useDispatch();

  useEffect(() => {
    if (checked) {
      setTimeout(() => {
        dispatch(resetErrorTimeLogin());
      }, 5000);
    } else {
      setTimeout(() => {
        dispatch(resetDocument());
      }, 6000);
    }
  }, []);

  if (checked) {
    return (
      <Fade in={open} timeout={4000}>
        <Alert variant="filled" severity="error" className="alert">
          {message}
        </Alert>
      </Fade>
    );
  }
  return (
    <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
      <Alert onClose={handleClose} severity="error" className="alert-success-mobile">
        {message}
      </Alert>
    </Snackbar>
  );
};
