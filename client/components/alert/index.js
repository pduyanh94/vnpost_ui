import React, { Component } from 'react';
import { connect } from 'react-redux';
import Fade from '@material-ui/core/Fade';
import Alert from '@material-ui/lab/Alert';

import { resetAlert } from '../../actions/common_action';

import './style.less';

class ReactAlert extends Component {
  state = {
    msg: '',
    showAlert: false,
    type: '',
  };

  componentDidMount() {
    const { childRef } = this.props;
    childRef(this);
  }

  componentWillUnmount() {
    const { childRef } = this.props;
    childRef(undefined);
  }

  show = (type, msg) => {
    this.setState({ msg, type, showAlert: true });
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { resetAlert } = this.props;
    if (this.state.showAlert) {
      setTimeout(() => {
        this.setState({ showAlert: false });
        resetAlert();
      }, 4000);
    }
  }

  render() {
    const { showAlert, msg, type } = this.state;
    return (
      <Fade in={showAlert} timeout={4000}>
        <Alert variant="filled" severity={type || 'error'} className="alert">
          {msg}
        </Alert>
      </Fade>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  resetAlert: () => dispatch(resetAlert()),
});

export default connect(null, mapDispatchToProps)(ReactAlert);
