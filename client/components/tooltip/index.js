import React from 'react';
import { makeStyles, Tooltip } from '@material-ui/core';

const useStylesBootstrap = makeStyles((theme) => ({
  arrow: {
    color: '#FFF',
  },
  tooltip: {
    backgroundColor: '#000',
  },
}));

export default function BootstrapTooltip(props) {
  const classes = useStylesBootstrap();

  return <Tooltip arrow classes={classes} {...props} />;
}
