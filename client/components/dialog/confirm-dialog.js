import React from 'react';
import PropTypes from 'prop-types';

import { useSelector } from 'react-redux';
import {
  Button,
  DialogActions,
  DialogContent,
  DialogContentText,
  Dialog,
  DialogTitle,
  makeStyles,
} from '@material-ui/core';
import closeIcon from '../../assets/icons/close.svg';
import warningIcon from '../../assets/icons/warning.png';

import './style.less';
import AppButton from '../button/AppButton';

ConfirmDialog.propTypes = {
  onYesClick: PropTypes.func,
  handleClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  message: PropTypes.string,
};

function ConfirmDialog(props) {
  const { open, handleClose, onYesClick, message, showOkButton } = props;

  const onNoClick = () => {
    handleClose();
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      className="confirm_dialog_container"
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      disableBackdropClick
    >
      <DialogTitle id="alert-dialog-title" className="dialog_title" onClose={handleClose}>
        {showOkButton ? 'Message' : 'Confirm Dialog'} <img onClick={handleClose} src={closeIcon} />
      </DialogTitle>
      <DialogContent className="dialog_content">
        <div className="img_container">
          <img src={warningIcon} />
        </div>
        <DialogContentText className="dialog_content_text" id="alert-dialog-description">
          {typeof message === 'object'
            ? message.map((msg, index) => (
                <p className="msg_text" key={`msg_confirm_${index}`}>
                  {msg}
                </p>
              ))
            : message}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <div className="dialog_action">
          <AppButton onClick={onYesClick} className="mr-8" buttonType="primary_yellow">
            {showOkButton ? 'Close' : 'Yes'}
          </AppButton>
          {!showOkButton && (
            <AppButton onClick={onNoClick} buttonType="secondary">
              No
            </AppButton>
          )}
        </div>
      </DialogActions>
    </Dialog>
  );
}

export default ConfirmDialog;
