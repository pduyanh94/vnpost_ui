import React, { PureComponent } from 'react';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import Typography from '@material-ui/core/Typography';
import AppButton from '../button/AppButton';
import CommonModal from '../modal/CommonModal';
class FormDialog extends PureComponent {
  state = {
    open: false,
  };
  handleClickOpen = () => {
    this.setState({
      open: true,
    });
  };

  handleClickClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {
    const {
      handleChange,
      buttonName,
      buttonClass,
      header,
      textAreaLabel,
      textAreaName,
      dialogButtonName,
      dialogButtonFunction,
      disableButton,
    } = this.props;
    return (
      <div className="form-dialog">
        <AppButton buttonType="outline" className={buttonClass} onClick={this.handleClickOpen}>
          {buttonName}
        </AppButton>
        <CommonModal
          open={this.state.open}
          onClose={this.handleClickClose}
          TitleComponent={() => (
            <Typography variant="h6">
              {header}
              <span className="mandatory">*</span>
            </Typography>
          )}
          aria-labelledby="form-dialog-title"
          className="adq-dialog"
        >
          <div className="content">
            <TextField
              name={textAreaName}
              onChange={handleChange}
              className="comments-box"
              rows={6}
              multiline
              variant="outlined"
              id={textAreaName}
              label={textAreaLabel}
              fullWidth
            />
          </div>
          <DialogActions className="actions" style={{ marginTop: 32 }}>
            <AppButton disabled={disableButton} onClick={dialogButtonFunction} buttonType="primary">
              {dialogButtonName}
            </AppButton>
            <AppButton onClick={this.handleClickClose} className="normal-btn" buttonType="secondary" tabIndex="0">
              Cancel
            </AppButton>
          </DialogActions>
        </CommonModal>
      </div>
    );
  }
}

export default FormDialog;
