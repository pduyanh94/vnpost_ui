import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';

import { tabsConfig } from '../../componentConfig';

class TabName extends Component {
  handleBulletPoints = (event) => {
    let el = event.target;
    let keycode = event.keyCode ? event.keyCode : event.which;

    if (el.value.trim() === '' && event.type === 'focus') {
      el.value = '• ';
    }
    if (event.type === 'blur') {
      if (el.value.trim() === '•' || el.value.trim() === '') {
        el.value = '';
      }
    }
    if (keycode === '32') {
      if (el.value.indexOf('*') === 0) {
        el.value = el.value.replace('*', '•');
      }
      el.value = el.value.replace('\n**', '\n\t◦');
      el.value = el.value.replace('\n*', '\n•');
    }
  };

  render() {
    const { tabName, prefix, validate, handleChange } = this.props;
    const renderTabs = tabsConfig.map((tab, idx) => {
      return (
        <Grid item md={12} sm={12} key={idx}>
          <FormControl variant="outlined">
            <TextField
              multiline={tab.multiline}
              error={validate && !this.props[tab.errorProp]}
              value={this.props[tab.value]}
              id={`${prefix}${tab.id}`}
              name={`${prefix}${tab.id}`}
              helperText={
                validate && !this.props[tab.errorProp]
                  ? 'This field is required!'
                  : ''
              }
              onChange={handleChange}
              onFocus={this[tab.focus]}
              onBlur={this[tab.blur]}
              onKeyUp={this[tab.keyUp]}
              label={tab.label}
              variant="outlined"
              rows={tab.rows}
            />
          </FormControl>
        </Grid>
      );
    });

    return (
      <Grid container spacing={2} className="tab-container">
        <Grid item md={12} sm={12}>
          <Typography variant="h6">{tabName}</Typography>
        </Grid>
        {renderTabs}
      </Grid>
    );
  }
}

export default TabName;
