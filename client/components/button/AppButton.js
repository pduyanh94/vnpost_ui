import { Button } from '@material-ui/core';
import React from 'react';
import './styles.less';

// buttonType: primary | secondary | outline

const AppButton = ({ text, buttonType, className, children, ...props }) => {
  return (
    <Button {...props} className={`app_button app_button_${buttonType} ${className}`}>
      {text || children}
    </Button>
  );
};

export default AppButton;
