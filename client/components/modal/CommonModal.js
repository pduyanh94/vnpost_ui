import { Dialog } from '@material-ui/core';
import React from 'react';
import closeBtn from './../../assets/icons/close-btn.svg';
import './styles.less';

const CommonModal = ({ title, TitleComponent, children, className, ...props }) => {
  return (
    <Dialog fullWidth disableBackdropClick maxWidth="sm" className={`common_modal ${className}`} {...props}>
      <div className="dialog_content">
        {TitleComponent && (
          <div className="title">
            <TitleComponent />
          </div>
        )}
        {title && <h5 className="title">{title}</h5>}
        <img className="cursor-pointer btn_close_modal" src={closeBtn} onClick={props.onClose} />
      </div>
      <div className="modal_content">{children}</div>
    </Dialog>
  );
};

export default CommonModal;
