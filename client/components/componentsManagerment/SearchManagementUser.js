import { FormControl, MenuItem, Select } from '@material-ui/core';
import React, { useState } from 'react';
import { BootstrapInput } from '../../componentConfig/StyledSelection';
import { StyledTextField } from '../../componentConfig/StyledTextField';
import './style.user.search.less';
import ClearIcon from '@material-ui/icons/Clear';
import AppButton from '../button/AppButton';

export const SearchManagementUser = ({ onChangeSearch, valueSearch }) => {
  const [value, setValue] = useState({
    firstName: valueSearch['firstName'] || '',
    phone: valueSearch['phone'] || '',
    role: valueSearch['role'] || '',
    asset: valueSearch['asset'] || '',
  });

  const getValueInputSearch = (event, keySearch) => {
    if (keySearch === 'firstName') {
      if (event.target.value.length === 50) {
        setValue({
          ...value,
          [keySearch]: event.target.value,
        });
        event.preventDefault();
      }
      if (event.target.value.length < 50) {
        setValue({
          ...value,
          [keySearch]: event.target.value,
        });
      }
    }

    if (keySearch === 'phone') {
      const regex = new RegExp('[0-9]');
      if (!regex.test(event.key)) {
        event.preventDefault();
      }
      if (event.target.value && event.target.value.length > 50) {
        event.preventDefault();
      } else {
        setValue({
          ...value,
          [keySearch]: event.target.value,
        });
      }
    }

    if (event.key === 'Enter') {
      onChangeSearch(value);
    } else {
      setValue({
        ...value,
        [keySearch]: event.target.value,
      });
    }
  };
  const handleSubmit = () => {
    onChangeSearch(value);
  };

  const handleChangeSelectRoleSearch = (event) => {
    setValue({
      ...value,
      role: event.target.value,
    });
  };
  const handleCleanTextSearch = (event, keySearch) => {
    event.target.value = '';
    setValue({
      ...value,
      [keySearch]: '',
    });
  };

  const handleOnPaste = (event, keySearch) => {
    if (keySearch === 'firstName') {
      const valueOfPaste = (event.clipboardData || window.clipboardData).getData('text');
      const totalValue = valueOfPaste + value.firstName;

      let subValueOfPaste;
      if (valueOfPaste.length > 50) {
        subValueOfPaste = valueOfPaste.substring(0, 50);
        setValue({
          ...value,
          [keySearch]: subValueOfPaste,
        });
        event.preventDefault();
      }
      if (totalValue && totalValue.length > 50) {
        subValueOfPaste = totalValue.substring(0, 50);
        setValue({
          ...value,
          [keySearch]: subValueOfPaste,
        });
        event.preventDefault();
      } else {
        setValue({
          ...value,
          [keySearch]: subValueOfPaste,
        });
      }
    }
  };

  return (
    <div className="search-container">
      <form className="form-search-management">
        <FormControl className="form-control-user-search">
          <label className="label-search" htmlFor="my-input-username">
          First Name
          </label>
          <StyledTextField
            className="styled-input-font-size"
            id="my-input-username"
            placeholder="Enter text"
            variant="outlined"
            value={value['firstName']}
            onChange={(event) => getValueInputSearch(event, 'firstName')}
            onKeyPress={(event) => getValueInputSearch(event, 'firstName')}
            onPaste={(event) => handleOnPaste(event, 'firstName')}
            InputProps={
              value['firstName'] !== ''
                ? {
                    endAdornment: (
                      <button
                        className="button-clean-text-search"
                        type="reset"
                        onClick={(event) => handleCleanTextSearch(event, 'firstName')}
                      >
                        <ClearIcon fontSize="small" />
                      </button>
                    ),
                  }
                : null
            }
          />
        </FormControl>

        <FormControl className="form-control-user-search">
          <label className="label-search" htmlFor="my-input-phone">
            Phone
          </label>
          <StyledTextField
            id="my-input-phone"
            className="styled-input-font-size"
            placeholder="Enter text"
            variant="outlined"
            value={value['phone']}
            onChange={(event) => getValueInputSearch(event, 'phone')}
            onKeyPress={(event) => getValueInputSearch(event, 'phone')}
            InputProps={
              value['phone'] !== ''
                ? {
                    endAdornment: (
                      <button
                        className="button-clean-text-search"
                        type="reset"
                        onClick={(event) => handleCleanTextSearch(event, 'phone')}
                      >
                        <ClearIcon fontSize="small" />
                      </button>
                    ),
                  }
                : null
            }
          />
        </FormControl>

        <FormControl className="form-control-user-search">
          <label className="label-search" htmlFor="my-input-role">
            Role
          </label>
          <Select
            labelId="user-management-search-select-label"
            className="selected-user-search"
            id="user-management-search-selected"
            value={value.role}
            onChange={handleChangeSelectRoleSearch}
            input={<BootstrapInput />}
          >
            <MenuItem value="">All</MenuItem>
            <MenuItem value="DU">Dashboard User</MenuItem>
            <MenuItem value="Approver">Approver</MenuItem>
            <MenuItem value="CIO">CIO</MenuItem>
          </Select>
        </FormControl>

        <FormControl className="form-control-user-search">
          <label className="label-search" htmlFor="my-input-assetCompany">
            Entity Name
          </label>
          <StyledTextField
            id="my-input-assetCompany"
            className="styled-input-font-size"
            placeholder="Enter text"
            variant="outlined"
            value={value['asset']}
            onChange={(event) => getValueInputSearch(event, 'asset')}
            onKeyPress={(event) => getValueInputSearch(event, 'asset')}
            InputProps={
              value['asset'] !== ''
                ? {
                    endAdornment: (
                      <button
                        className="button-clean-text-search"
                        type="reset"
                        onClick={(event) => handleCleanTextSearch(event, 'asset')}
                      >
                        <ClearIcon fontSize="small" />
                      </button>
                    ),
                  }
                : null
            }
          />
        </FormControl>

        <FormControl className="btn-search-styled">
          <AppButton buttonType="primary" onClick={() => handleSubmit()}>
            Search
          </AppButton>
        </FormControl>
      </form>
    </div>
  );
};
