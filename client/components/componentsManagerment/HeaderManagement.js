import React from 'react';
import PropTypes from 'prop-types';
import { Hidden } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import './style.less';
import AppButton from '../button/AppButton';

export const HeaderManagement = ({ title, handleOpenDialog, addButtonText = 'Add New' }) => {
  return (
    <div className="title_managerment">
      <div className="title_management_header">
        <span>{title}</span>
        <br></br>
        <h3 className="title">{title}</h3>
      </div>
      {handleOpenDialog && (
        <Hidden only="xs">
          <div>
            <AppButton buttonType="outline" onClick={handleOpenDialog}>
              <AddIcon /> {addButtonText}
            </AppButton>
          </div>
        </Hidden>
      )}
    </div>
  );
};

HeaderManagement.propTypes = {
  handleOpenDialog: PropTypes.func,
  title: PropTypes.string,
  addButtonText: PropTypes.string,
};

HeaderManagement.defaultProps = {
  title: '',
  addButtonText: 'Add New',
};
