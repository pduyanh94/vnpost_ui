import React, { useEffect, useState } from 'react';
import { FormControl, Grid, Hidden } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';
import PropTypes from 'prop-types';
import {  DatePicker  } from  'antd';
import moment from 'moment';
import { StyledTextField } from '../../componentConfig/StyledTextField';
import AppButton from '../button/AppButton';
import MuiSelect from '../../components/create-field/mui-select';
import './style.less';

const validateFieldMaxlength50 = [
  'reviewer',
  'cluster',
  'clusterName',
  'reviewerName',
  'assetName',
  'ceoName',
  'chairmanName',
];

export const SearchManagement = ({ fieldInputs = [], onChangeSearch, allDisable=false }) => {
  const [value, setValue] = useState({});
  useEffect(() => {
    const defaultValue = {};
    fieldInputs.forEach((field) => {
      defaultValue[field.keySearch] = '';
    });
    setValue(defaultValue);
  }, []);

  const setValueHelper = (field, data) => {
    setValue({
      ...value,
      [field]: data,
    });
  };

  const getValueInputSearch = ({ target }, fieldConfig) => {
    const { value = '' } = target;
    const { keySearch, maxLength, isNumber } = fieldConfig;
    if (isNumber && isNaN(value)) {
      return;
    }

    if (maxLength && value.length > maxLength) {
      setValueHelper(keySearch, value.slice(0, maxLength));
      return;
    }

    if (validateFieldMaxlength50.indexOf(keySearch) !== -1) {
      if (value.length < 50) {
        setValueHelper(keySearch, value);
      }
    } else if (keySearch === 'description') {
      if (value.length < 255) {
        setValueHelper(keySearch, value);
      }
    } else {
      setValueHelper(keySearch, value);
    }
  };

  const handleKeyPress = ({ key }) => {
    if (key === 'Enter') {
      onChangeSearch(value);
    }
  };

  const handleSubmit = () => {
    console.log(value, 3333)
    onChangeSearch(value);
  };

  const handleCleanTextSearch = (event, keySearch) => {
    setValue({
      ...value,
      [keySearch]: '',
    });
  };

  const handleChangeSelectionField = (e) => {
    setValueHelper(keySearch, e.target.value);
  };

  const onChangeDate = (target, el) => {

    const value = target;
    const { keySearch } = el;
    setValueHelper(keySearch, moment(value).toISOString());

  }
  return (
    <div className="search-container">
      <form className="form-search-managerment" onSubmit={(e) => e.preventDefault()}>
        <Grid container>
          <div className={`search-form flex-${fieldInputs.length}`}>
            {fieldInputs.length > 0
              ? fieldInputs.map((el, index) => {
                  let classContainer = '';
                  if (index > 0) {
                    classContainer += ' display_sm_none';
                  }
                  if (index === fieldInputs.length - 1 && fieldInputs.length > 1) {
                    classContainer += ' mr-md-0';
                  }

                  return (
                    <FormControl key={index} className={`input-field ${classContainer}`}>
                      <label htmlFor={`my-input-${index}`} className="label-search">
                        {el.label}
                      </label>
                      {el.inputType === 'DATETIME' ? (
                        <DatePicker onChange={(e) => onChangeDate(e, el)}  placeholder={el.placeHolder || 'Enter text'}/>
  
                      ) : (
                        <StyledTextField
                          id={`my-input-${index}`}
                          className="input"
                          placeholder={el.placeHolder || 'Enter text'}
                          variant="outlined"
                          value={value[`${el.keySearch}`]}
                          onChange={(event) => getValueInputSearch(event, el)}
                          onKeyPress={handleKeyPress}
                          disabled={allDisable}
                          InputProps={
                            value[el.keySearch] && value[el.keySearch].length > 0
                              ? {
                                  endAdornment: (
                                    <button
                                      className="button-clean-text-search"
                                      type="reset"
                                      onClick={(event) => handleCleanTextSearch(event, el.keySearch)}
                                    >
                                      <ClearIcon fontSize="small" />
                                    </button>
                                  ),
                                }
                              : null
                          }
                        />
                      )}
                    </FormControl>
                  );
                })
              : null}
          </div>
          <div className={`btn_container flex-${fieldInputs.length}`}>
            <AppButton buttonType="primary" onClick={() => handleSubmit()} disabled={allDisable}>
              Search
            </AppButton>
          </div>
        </Grid>
      </form>
    </div>
  );
};

SearchManagement.proTypes = {
  fieldInputs: PropTypes.array.isRequired,
  onChangeSearch: PropTypes.func.isRequired,
};
