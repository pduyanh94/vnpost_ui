import React, { useEffect, useState } from 'react';
import 'date-fns';
import { BootstrapInput } from '../../componentConfig/StyledSelection';
import { FormControl, Grid, MenuItem, Select } from '@material-ui/core';
import { StyledTextField } from '../../componentConfig/StyledTextField';
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import ClearIcon from '@material-ui/icons/Clear';
import './style.key_business.less';
import { StyledButton } from '../../componentConfig/StyledButton';
import { useSelector } from 'react-redux';
import { formatMonthSelect } from '../../componentConfig';

export const SearchKeyBusiness = ({
  onChangeSearch,
  listOption,
  valueSearch,
}) => {
  const [value, setValue] = useState({
    clusterName: valueSearch['clusterName'] || '',
    period: null,
    createdByUsername: valueSearch['createdByUsername'] || '',
  });

  const { listClusters, loadGetListCluster } = useSelector(
    (state) => state.clusterGetList,
  );

  const optionClusters = listClusters
    ? listClusters.reduce((acc, row, index) => {
        acc.push(row.name);
        return acc;
      }, [])
    : [];



  const handleCleanTextSearch = (event, keySearch) => {
    setValue({
      ...value,
      [keySearch]: '',
    });
  };
  const getValueInputSearch = (event, keySearch) => {
    if (event.target.value && event.target.value.length >= 50) {
      event.preventDefault();
    }
    setValue({
      ...value,
      [keySearch]: event.target.value.trim(),
    });
    if (event.key === 'Enter') {
      onChangeSearch(value);
    }
  };

  const handleSubmit = () => {
    const valueNewSearch = {
      clusterName: value.clusterName,
      period: value.period
        ? new Intl.DateTimeFormat('en', {
            month: 'numeric',
            year: 'numeric',
          }).format(value.period)
        : '',
      createdByUsername: value.createdByUsername,
    };

    onChangeSearch(valueNewSearch);
  };

  const handleChangeSelectClusterReportSearch = (event) => {
    setValue({
      ...value,
      clusterName: event.target.value,
    });
  };

  const handleOnPaste = (event, keySearch) => {
    
    const valueOfPaste = (event.clipboardData || window.clipboardData).getData(
      'text',
    );
    let subValueOfPaste;
    const totalValue = valueOfPaste + value.createdByUsername;
    if (valueOfPaste.length > 50) {
      subValueOfPaste = valueOfPaste.substring(0, 50);

      setValue({
        ...value,
        [keySearch]: subValueOfPaste,
      });

      event.preventDefault();
    } 

    if(totalValue && totalValue.length > 50) {
      subValueOfPaste = totalValue.substring(0, 50);
      setValue({
        ...value,
        [keySearch]: subValueOfPaste,
      });

      event.preventDefault();
    }
    
    
    else {
      setValue({
        ...value,
        [keySearch]: valueOfPaste,
      });
    }
  };

  return (
    <div className="search-container">
      <form className="form-search-cluster-report">
        <FormControl className="form-control-cluster-report-search report-cluster-name">
          <label className="label-search" htmlFor="my-input-cluster-selection">
            Cluster
          </label>
          <Select
            labelId="cluster-report-search-select-label"
            className="selected-user-search"
            id="my-input-cluster-selection"
            value={value.clusterName}
            defaultValue={valueSearch.clusterName}
            onChange={handleChangeSelectClusterReportSearch}
            input={<BootstrapInput />}
          >
            <MenuItem
              value=""
              className="item-selected-cluster-report"
              style={{ fontSize: '12px' }}
            >
              All
            </MenuItem>
            {optionClusters &&
              optionClusters.map((item, index) => (
                <MenuItem
                  value={item}
                  key={index}
                  className="item-selected-cluster-report"
                  style={{ fontSize: '12px' }}
                >
                  {item}
                </MenuItem>
              ))}
          </Select>
        </FormControl>

        <FormControl className="form-control-cluster-report-search hidden-mobile">
          <label className="label-search" htmlFor="my-input-period">
            Period
          </label>

          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container>
              <KeyboardDatePicker
                autoOk
                id="my-input-period"
                className="selected-period-search"
                placeholder="Enter text"
                inputVariant="outlined"
                variant="inline"
                value={value.period}
                // defaultValue={value['period']}
                format="MM/yyyy"
                views={['month', 'year']}
                margin="normal"
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
                onChange={(date) => setValue({ ...value, period: date })}
              />
            </Grid>
          </MuiPickersUtilsProvider>
        </FormControl>

        <FormControl className="form-control-cluster-report-search hidden-mobile">
          <label className="label-search" htmlFor="my-input-createdBy">
            Created by
          </label>
          <StyledTextField
            id="my-input-createdBy"
            placeholder="Enter text"
            variant="outlined"
            value={value['createdByUsername']}
            // defaultValue={value['createdBy']}
            onChange={(event) =>
              getValueInputSearch(event, 'createdByUsername')
            }
            onKeyPress={(event) =>
              getValueInputSearch(event, 'createdByUsername')
            }
            onPaste={(event) => handleOnPaste(event, 'createdByUsername')}
            InputProps={
              value.createdByUsername && value.createdByUsername.length > 0
                ? {
                    endAdornment: (
                      <button
                        className="button-clean-text-search"
                        type="reset"
                        onClick={(event) =>
                          handleCleanTextSearch(event, 'createdByUsername')
                        }
                      >
                        <ClearIcon fontSize="small" />
                      </button>
                    ),
                  }
                : null
            }
          />
        </FormControl>
        <FormControl className="btn-search-cluster">
          <StyledButton className="btn-search" onClick={() => handleSubmit()}>
            Search
          </StyledButton>
        </FormControl>
      </form>
    </div>
  );
};
