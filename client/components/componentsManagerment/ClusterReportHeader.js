import { Button } from '@material-ui/core';
import React from 'react';
import './style.cluster_report_header.less';
export const ClusterReportHeader = ({ titleHeader }) => {
  return (
    <div>
      <div className="head-asset-report-create">
        <div>
          <span>{titleHeader}</span>
          <span>{titleHeader}</span>
        </div>
        <div>
          <Button
            className="save-create-cluster-report"
            //   onClick={() => history.push('/key-business/create')}
          >
            Save
          </Button>
          <Button className="cancel-cluster-report">Cancel</Button>
        </div>
      </div>

      
    </div>
  );
};
