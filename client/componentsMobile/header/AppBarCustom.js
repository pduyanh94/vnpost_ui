import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';

import _ from 'lodash';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import {
  // NotificationsNone as NotificationsNoneIcon,
  Menu as MenuIcon,
  ExitToApp as ExitToAppIcon,
} from '@material-ui/icons';
import {
  Grid,
  ListItemText,
  Menu,
  MenuItem,
  Container,
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Badge,
  Tooltip,
  List,
  Popover,
} from '@material-ui/core';
import moment from 'moment';

import { connectWebSocket } from '../../utils/index';
import { clearDataOnLogout } from '../../actions/common_action';

import './style.less';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: 'white',
    '& .MuiBadge-anchorOriginTopRightRectangle': {
      top: '2px',
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    color: 'black',
  },
  title: {
    flexGrow: 1,
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 14,
  },
  styleHeader: {
    backgroundColor: 'white',
  },
  notification: {
    color: 'black',
  },
}));

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
    top: '56px',
    left: '0px !important',
  },
})((props) => {
  return (
    <Menu
      elevation={0}
      getContentAnchorEl={null}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      PaperProps={{
        style: {
          top: 56,
          left: '0!important',
          maxWidth: '100%',
          width: '100%',
        },
      }}
      top={0}
      {...props}
    />
  );
});

const StyledMenuItem = withStyles(() => ({
  root: {
    '& .MuiListItemText-root': {
      marginTop: '0px',
      marginBottom: '0px',
      span: {
        fontSize: '12px',
        fontWeight: 'bold',
      },
    },

    textDecoration: 'none',
  },
}))(MenuItem);

export const AppBarCustom = ({ titleScreen, ...props }) => {
  const history = useHistory();

  const { pathname } = useLocation();
  const { appToken } = useSelector((state) => state.userReducer);
  const [receiverId, setReceiverId] = React.useState();
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const dispatch = useDispatch();
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const [anchorNoti, setAnchorNoti] = React.useState(null);
  const [openNoti, setOpenNoti] = React.useState(false);
  const [countNotification, setCountNotification] = React.useState(0);
  const [options, setOptions] = React.useState([]);

  const handleClickNoti = (event) => {
    setAnchorNoti(event.currentTarget);
    setOpenNoti(true);
    setCountNotification(0);
    const socket = connectWebSocket(receiverId);
    socket.emit('client.emit.updateSeenCommentTask', receiverId);
  };

  const handleCloseNoti = () => {
    setAnchorNoti(null);
    setOpenNoti(false);
  };

  const handleLogout = () => {
    localStorage.removeItem('reporting-ui-user-token');
    localStorage.removeItem('powerbi-access-token');
    localStorage.removeItem('powerbi-refresh_token');
    localStorage.removeItem('powerbi-expires-on');
    localStorage.removeItem('user');
    dispatch(clearDataOnLogout());
    history.push('/');
  };

  React.useEffect(() => {
    setReceiverId(JSON.parse(localStorage.getItem('user')).userId);
    const socket = connectWebSocket(receiverId);

    socket.emit('client.emit.getCommentTaskList', {
      filter: {
        pagination: { currentPage: 1, limit: 30 },
        order: { orderBy: 'createdDate', direction: 1 },
        search: { reviewerId: receiverId },
      },
    });

    socket.on('server.emit.getCommentTaskList', (data) => {
      if (data && data.results && data.results.length > 0) {
        setCountNotification(data.options.unseenCount);
        setOptions(data.results);
      }
    });

    socket.on('server.emit.update', () => {
      socket.emit('client.emit.getCommentTaskList', {
        filter: {
          pagination: { currentPage: 1, limit: 30 },
          order: { orderBy: 'createdDate', direction: 1 },
          search: { reviewerId: receiverId },
        },
      });
    });
  }, [appToken, receiverId]);

  const handleRedirectPage = (uri) => {
    handleClose();
    history.push(uri);
  };

  return (
    <div className={classes.root}>
      <Grid item xs={12}>
        <AppBar position="sticky">
          <Toolbar className={classes.styleHeader}>
            <div>
              <IconButton
                edge="start"
                className={classes.menuButton}
                color="inherit"
                aria-label="menu"
                onClick={handleClick}
              >
                <MenuIcon />
              </IconButton>

              <StyledMenu id="lock-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
                <StyledMenuItem
                  onClick={() => handleRedirectPage('/')}
                  className={`style-header-mobile ${pathname === '/' ? 'active-url' : ''}`}
                >
                  <ListItemText primary="Dashboard" className="style-header-mobile-ul" />
                </StyledMenuItem>

                <StyledMenuItem
                  onClick={() => handleRedirectPage('/users')}
                  className={`style-header-mobile ${pathname === '/users' ? 'active-url' : ''}`}
                >
                  <ListItemText primary="User Management" className="style-header-mobile-ul" />
                </StyledMenuItem>

                <StyledMenuItem
                  onClick={() => handleRedirectPage('/assets')}
                  className={`style-header-mobile ${pathname === '/assets' ? 'active-url' : ''}`}
                >
                  <ListItemText primary="Asset Management" className="style-header-mobile-ul" />
                </StyledMenuItem>

                <StyledMenuItem
                  onClick={() => handleRedirectPage('/clusters')}
                  className={`style-header-mobile ${pathname === '/clusters' ? 'active-url' : ''}`}
                >
                  <ListItemText primary="Cluster Management" className="style-header-mobile-ul" />
                </StyledMenuItem>

                {/* <StyledMenuItem
                  onClick={() => handleRedirectPage('/documents')}
                  className={`style-header-mobile ${pathname === '/documents' ? 'active-url' : ''}`}
                >
                  <ListItemText primary="Document Management" className="style-header-mobile-ul" />
                </StyledMenuItem> */}

                {/* <StyledMenuItem
                  onClick={() => handleRedirectPage('/key-business')}
                  className={`style-header-mobile ${pathname === '/key-business' ? 'active-url' : ''}`}
                >
                  <ListItemText primary="Cluster Report" className="style-header-mobile-ul" />
                </StyledMenuItem> */}

                <StyledMenuItem
                  onClick={() => handleRedirectPage('/scorecard')}
                  className={`style-header-mobile ${pathname === '/scorecard' ? 'active-url' : ''}`}
                >
                  <ListItemText primary="Scorecard Management" className="style-header-mobile-ul" />
                </StyledMenuItem>
              </StyledMenu>
            </div>

            <Typography variant="h6" className={classes.title}>
              {titleScreen}
            </Typography>
          </Toolbar>
        </AppBar>
      </Grid>
    </div>
  );
};

function calDurationTime(time) {
  let currentTime = moment(new Date(), 'YYYY-MM-DDTHH:mm');
  let startTime = moment(time, 'YYYY-MM-DDTHH:mm');
  let minute = moment.duration(currentTime.diff(startTime)).asMinutes();
  let duration = '';
  if (minute == 0) {
    duration = 'now';
  } else if (minute > 0 && minute < 60) {
    duration = 'last ' + Math.round(minute) + ' minute(s)';
  } else {
    duration = moment(time).format('DD-MMMM-YYYY') + ' at ' + moment(time).format('HH:mm');
  }
  return duration;
}
