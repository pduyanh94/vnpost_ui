import React, { useState } from 'react';
import { FormControl } from '@material-ui/core';
import PropTypes from 'prop-types';
import { StyledButtonSearchMobile } from '../../componentConfig/StyledButton';
import { StyledTextField } from '../../componentConfig/StyledTextField';
import ClearIcon from '@material-ui/icons/Clear';
import AppButton from '../../components/button/AppButton';

import './style.less';

export const SearchManagementMobile = ({ fieldInputs, onChangeSearch, valueSearch, roleScreen }) => {
  const [value, setValue] = useState({
    username: valueSearch || '',
    assetName: valueSearch || '',
    clusterName: valueSearch || '',
  });
  const getValueInputSearch = (event, keySearch) => {
    if (event.target.value.length === 50) {
      setValue({
        ...value,
        [keySearch]: event.target.value.trim(),
      });
      event.preventDefault();
    }
    if (event.target.value.length < 50) {
      setValue({
        ...value,
        [keySearch]: event.target.value.trim(),
      });
    }

    if (event.key === 'Enter') {
      event.preventDefault();
      onChangeSearch(value);
    }
  };

  const handleSubmit = () => {
    onChangeSearch(value);
  };

  const handleCleanTextSearch = (event, keySearch) => {
    setValue({
      ...value,
      [keySearch]: '',
    });
  };

  const handleOnPaste = (event, keySearch) => {
    const valueOfPaste = (event.clipboardData || window.clipboardData).getData('text');
    const totalValue = valueOfPaste + value[`${keySearch}`];

    let subValueOfPaste;
    if (valueOfPaste.length > 50) {
      subValueOfPaste = valueOfPaste.substring(0, 50);
      setValue({
        ...value,
        [keySearch]: subValueOfPaste,
      });
      event.preventDefault();
    }
    if (totalValue && totalValue.length > 50) {
      subValueOfPaste = totalValue.substring(0, 50);
      setValue({
        ...value,
        [keySearch]: subValueOfPaste,
      });
      event.preventDefault();
    } else {
      setValue({
        ...value,
        [keySearch]: subValueOfPaste,
      });
    }
  };

  return (
    <div className="search-container">
      <form className="form-search-managerment">
        <FormControl
          key={fieldInputs.keySearch}
          style={{ marginRight: '12px', width: '135%' }}
          className="form_control_input"
        >
          <label htmlFor={`my-input-${fieldInputs.keySearch}`} className="label-search">
            {fieldInputs.label}
          </label>

          <StyledTextField
            id={`my-input-${fieldInputs.keySearch}`}
            placeholder="Enter Text"
            variant="outlined"
            value={value[`${fieldInputs.keySearch}`]}
            onChange={(event) => getValueInputSearch(event, fieldInputs.keySearch)}
            onKeyPress={(event) => getValueInputSearch(event, fieldInputs.keySearch)}
            onPaste={(event) => handleOnPaste(event, fieldInputs.keySearch)}
            InputProps={
              value[fieldInputs.keySearch] && value[fieldInputs.keySearch].length > 0
                ? {
                    endAdornment: (
                      <button
                        type="reset"
                        className="button-clean-text-search"
                        onClick={(event) => handleCleanTextSearch(event, fieldInputs.keySearch)}
                      >
                        <ClearIcon fontSize="small" />
                      </button>
                    ),
                  }
                : null
            }
          />
        </FormControl>
        <AppButton className="btn-search" buttonType="primary" onClick={handleSubmit}>
          Search
        </AppButton>
      </form>
    </div>
  );
};

SearchManagementMobile.proTypes = {
  fieldInputs: PropTypes.object.isRequired,
  onChangeSearch: PropTypes.func.isRequired,
};
