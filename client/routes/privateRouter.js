import React from 'react'
import { Redirect, Route } from 'react-router'

export const PrivateRouter = ({component: Component, isLogin, ...rest}) => {
    return (
        <Route
            {...rest}
            render={(props)=>{
                localStorage.getItem("powerbi-access-token")
                isLogin ? (<Component {...props}>{props.children}</Component>) : (<Redirect to="/login"/>)
            }}
        />
            
        
    )
}
