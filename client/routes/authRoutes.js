import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import PageNotFound from '../components/pageNotFound';
import asyncComponent from '../hoc/asyncComponent';

const AsyncCompanyList = asyncComponent(() => import('../containers/companyList'));
const authRoutes = () => {
  // const { role } = JSON.parse(localStorage.getItem('user'));

  return (
    <Switch>
      <Route exact path="/" component={AsyncCompanyList} />
      <Route render={() => <PageNotFound />} />
    </Switch>
  );
};

export default authRoutes;
