import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Login from '../containers/login';
import PageNotFound from '../components/pageNotFound';

const unauthRoutes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Login} />
      <Route render={() => <PageNotFound />} />
    </Switch>
  );
};

export default unauthRoutes;
