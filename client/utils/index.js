import socketIOClient from 'socket.io-client';
const ENDPOINT = `/notifications`;

export const upperCaseFirstLetter = (string) =>
  `${string.charAt(0).toUpperCase()}${string.slice(1)}`;

export const connectWebSocket = (userId) => {
  const token = localStorage.getItem('powerbi-access-token');
  const socket = socketIOClient(ENDPOINT, {
    'force new connection': true,
    reconnectionAttempts: 'Infinity',
    timeout: 10001,
    transports: ['websocket', 'polling'],
  });
  //socket.on('connect', () => {});
  socket.emit('client.emit.authenticate', {
    authData: {
      token: token,
    },
    userId: userId,
  });
  return socket;
};
