import {
  SAVE_COMPANY_DETAILS,
  SAVE_UPDATE_STATUS,
  RESET_COMPANY_DETAILS,
  SAVE_COMPANY_LIST,
} from './actionTypes';

export const saveCompanyDetails = (data) => ({
  type: SAVE_COMPANY_DETAILS,
  data,
});

export const saveCompanyList = (data) => ({ type: SAVE_COMPANY_LIST, data });

export const saveUpdateStatus = (data) => ({ type: SAVE_UPDATE_STATUS, data });

export const resetCompanyDetails = (data) => ({ type: RESET_COMPANY_DETAILS });
