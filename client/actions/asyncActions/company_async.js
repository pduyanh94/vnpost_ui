import { webApiGet } from '../../services/webBaseApi';
import { saveCompanyList } from '../company_action';
import { enableDisableLoader, setErrorAlert } from '../common_action';


export const getCompanyList = ({ search }) => async (dispatch) => {
  dispatch(enableDisableLoader(true));
  const { startDate = '', endDate = '', userName = '', passWord = '', databaseName = '', databaseURL = '' } = search;
  try {
    const URL = `/entity/assetCompanyList?filter={"search":{"startDate":"${startDate}","endDate":"${endDate}", "userName": "${userName}", "passWord": "${passWord}", "databaseName": "${databaseName}", "databaseURL": "${databaseURL}"}}`;
    const response = await webApiGet(URL).request;

    dispatch(enableDisableLoader(false));
    const {
      data: { data },
      status,
    } = response;
    console.log(data);
    if (status === 200) {
      dispatch(saveCompanyList({ posts: data }));
    }
  } catch (error) {
    dispatch(enableDisableLoader(false));
    dispatch(saveCompanyList({ posts: [] }));
    if (error && error.response && error.response.data) {
      dispatch(setErrorAlert(true, error.response.data.error.message));
    }
  }
};
