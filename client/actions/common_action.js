import {
  ENABLE_DISABLE_LOADER,
  SHOW_ERROR_ALERT,
  RESET_ALERT,
  CLEAR_DATA_ON_LOGOUT,
} from './actionTypes';

export const enableDisableLoader = (show) => ({
  type: ENABLE_DISABLE_LOADER,
  show,
});

export const setErrorAlert = (show, message) => ({
  type: SHOW_ERROR_ALERT,
  show,
  message,
});

export const resetAlert = () => ({ type: RESET_ALERT });

export const clearDataOnLogout = () => ({ type: CLEAR_DATA_ON_LOGOUT });
