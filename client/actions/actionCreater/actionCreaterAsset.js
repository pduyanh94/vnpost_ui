import * as actionType from '../actionTypes';

export const getAssetManagement = (param) => {
  return {
    type: actionType.GET_LIST_ASSET,
    payload: param,
  };
};
export const getAssetManagementSuccess = ({
  listAsset,
  currentPage,
  totalPage,
  limit,
  totalRecord,
}) => {
  return {
    type: actionType.GET_LIST_ASSET_SUCCESS,
    payload: { listAsset, currentPage, totalPage, limit, totalRecord },
  };
};
export const getAssetManagementError = (error) => {
  return {
    type: actionType.GET_LIST_ASSET_FAILURE,
    payload: error,
  };
};

export const deletedAsset = (param) => {
  return {
    type: actionType.DELETED_ASSET,
    payload: param,
  };
};

export const deletedAssetSuccess = () => {
  return {
    type: actionType.DELETED_ASSET_SUCCESS,
  };
};
export const deletedAssetError = (error) => {
  return {
    type: actionType.DELETED_ASSET_FAILURE,
    payload: error,
  };
};
