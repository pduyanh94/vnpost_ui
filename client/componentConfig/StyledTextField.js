import { TextField, withStyles } from '@material-ui/core'


export const StyledTextField = withStyles({
    root: {
        "& .MuiOutlinedInput-input": {
            padding: "18.5px 14px"
        },
        "& .MuiOutlinedInput-root": {
            borderRadius: '0px',
            height: '40px'
        },
        "& MuiInputBase-root":{
            height: '40px',
        },
        width: '100%',       
    }

})(TextField)
