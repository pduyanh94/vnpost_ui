import { Button, withStyles } from '@material-ui/core';

export const StyleButtonSearch = withStyles({
  root: {
    borderRadius: '0px',
    textTransform: 'none',
    padding: '0px !important',
    backgroundColor: '#00205C',
  },
})(Button);

export const StyledButtonMobile = withStyles({
  root: {
    borderRadius: '6px',
    textTransform: 'none',
    backgroundColor: '#00205C',
    width: '100%',
    color: 'white',
    height: '40px',
    '&:focus': {
      backgroundColor: '#00205C',
    },
  },
})(Button);

export const StyledButtonSearchMobile = withStyles({
  root: {
    borderRadius: '0px',
    textTransform: 'none',
    backgroundColor: '#00205C',
    height: '40px',
    marginRight: '31px',
    width: '100%',
  },
})(Button);

export const StyledButton = withStyles({
  root: {
    borderRadius: '0px',
    textTransform: 'none',
    height: '40px',
  },
})(Button);

export const StyledButtonAddNew = withStyles({
  root: {
    borderRadius: '0px',
    height: '40px',
    width: '125px',
    textTransform: 'capitalize',
    backgroundColor: '#6AAAE4',
    color: 'white',
    fontSize: '12px',
    display: 'flex',
    flexDirection: 'column',

    alignItems: 'center',

    // textDecoration: ''
  },

  contained: {
    boxShadow: 'none',
  },
})(Button);
