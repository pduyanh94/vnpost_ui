import React from 'react';
import BootstrapTooltip from '../components/tooltip';

export const companyInformationTabs = [
  {
    tabName: 'Key Business Developments',
    prefix: 'kbd',
    leftHeader: 'kbdLeftHeader',
    leftContent: 'kbdLeftContent',
    rightHeader: 'kbdRightHeader',
    rightContent: 'kbdRightContent',
  },
  {
    tabName: 'Financial Performance Insights',
    prefix: 'fpi',
    leftHeader: 'fpiLeftHeader',
    leftContent: 'fpiLeftContent',
    rightHeader: 'fpiRightHeader',
    rightContent: 'fpiRightContent',
  },
  {
    tabName: 'Overview',
    prefix: 'ow',
    leftHeader: 'owLeftHeader',
    leftContent: 'owLeftContent',
    rightHeader: 'owRightHeader',
    rightContent: 'owRightContent',
  },
];

export const tabsConfig = [
  {
    multiline: false,
    errorProp: 'leftHeader',
    value: 'leftHeader',
    id: 'LeftHeader',
    helperText: 'This field is required!',
    focus: '',
    blur: '',
    keyUp: '',
    label: 'Left Header',
    rows: '',
  },
  {
    multiline: true,
    errorProp: 'leftContent',
    value: 'leftContent',
    id: 'LeftContent',
    helperText: 'This field is required!',
    focus: 'handleBulletPoints',
    blur: 'handleBulletPoints',
    keyUp: 'handleBulletPoints',
    label: 'Left Content',
    rows: 6,
  },
  {
    multiline: false,
    errorProp: 'rightHeader',
    value: 'rightHeader',
    id: 'RightHeader',
    helperText: 'This field is required!',
    focus: '',
    blur: '',
    keyUp: '',
    label: 'Right Header',
    rows: '',
  },
  {
    multiline: true,
    errorProp: 'rightContent',
    value: 'rightContent',
    id: 'RightContent',
    helperText: 'This field is required!',
    focus: 'handleBulletPoints',
    blur: 'handleBulletPoints',
    keyUp: 'handleBulletPoints',
    label: 'Right Content',
    rows: 6,
  },
];

export const headCellsDataValidation = [
  { id: 'stt', applySorting: false, label: 'No', width: 30 },
  { id: 'Itemcode', label: 'Mã bưu gửi', value: 'ItemCode', applySorting: false },
  { id: 'CustomerCode', label: 'Mã khách hàng', applySorting: false },
];

export const monthMap = {
  Jan: 'January',
  Feb: 'February',
  Mar: 'March',
  Apr: 'April',
  May: 'May',
  Jun: 'June',
  Jul: 'July',
  Aug: 'August',
  Sep: 'September',
  Oct: 'October',
  Nov: 'November',
  Dec: 'December',
};

export const headCellsUsers = [
  { id: 'stt', numeric: true, disablePadding: true, applySorting: false, label: 'No', width: 30 },
  {
    id: 'firstName',
    numeric: false,
    label: 'First Name',
    applySorting: true,
    disablePadding: false,
    width: 150,
  },
  {
    id: 'lastName',
    numeric: false,
    label: 'Last Name',
    applySorting: true,
    disablePadding: false,
    width: 150,
  },
  {
    id: 'phone',
    numeric: false,
    label: 'Phone',
    applySorting: false,
    disablePadding: false,
    width: 100,
  },
  {
    id: 'email',
    numeric: false,
    label: 'Email',
    applySorting: true,
    disablePadding: false,
    width: 150,
  },
  {
    id: 'role',
    numeric: false,
    label: 'Role',
    applySorting: true,
    disablePadding: false,
    width: 100,
  },
  {
    id: 'asset',
    numeric: false,
    label: 'Entity Name',
    applySorting: false,
    disablePadding: false,
    width: 100,
    render: (row) => {
      let content = `${row.cluster || ''}${row.cluster && row.asset ? ', ' : ''}${row.asset || ''}`;

      return (
        <BootstrapTooltip title={content} placement="top-start">
          <p className="table_cell_custom">{content}</p>
        </BootstrapTooltip>
      );
    },
  },
  {
    id: 'action',
    numeric: false,
    label: 'Action',
    applySorting: false,
    disablePadding: false,
    width: 100,
  },
];

export const headCellsAssets = [
  { id: 'stt', numeric: true, label: 'No', applySorting: false, disablePadding: true, width: 30 },
  {
    id: 'assetName',
    numeric: false,
    label: 'Asset Name',
    applySorting: true,
    disablePadding: false,
    width: 120,
  },
  {
    id: 'ceoName',
    numeric: false,
    label: 'CEO Name',
    applySorting: true,
    disablePadding: false,
    width: 150,
  },
  {
    id: 'ceoContact',
    numeric: true,
    label: 'CEO contact',
    applySorting: false,
    disablePadding: false,
    width: 120,
  },
  {
    id: 'chairmanName',
    numeric: false,
    label: 'Chairmain Name',
    applySorting: true,
    disablePadding: false,
    width: 150,
  },
  {
    id: 'chairmanContact',
    numeric: true,
    label: 'Chairmain contact',
    applySorting: false,
    disablePadding: false,
    width: 120,
  },
  {
    id: 'cluster',
    numeric: false,
    label: 'Cluster',
    applySorting: true,
    disablePadding: false,
    width: 150,
  },
  {
    id: 'action',
    numeric: false,
    label: 'Action     ',
    applySorting: true,
    disablePadding: false,
    width: 100,
  },
];

export const headCellsClusters = [
  {
    id: 'stt',
    numeric: true,
    label: 'No',
    applySorting: false,
    disablePadding: true,
    width: 30,
  },
  {
    id: 'name',
    numeric: false,
    label: 'Cluster Name',
    applySorting: true,
    disablePadding: false,
    width: 200,
  },
  {
    id: 'description',
    numeric: false,
    label: 'Description',
    applySorting: true,
    disablePadding: false,
    width: 200,
  },
  {
    id: 'action',
    numeric: false,
    label: 'Action',
    applySorting: false,
    disablePadding: false,
    width: 100,
  },
];

export const arrayOptionSearchCompanyInfo = [
  {
    label: 'DB url',
    keySearch: 'databaseURL',
  },
  {
    label: 'DB name',
    keySearch: 'databaseName',
  },
  {
    label: 'User Name',
    keySearch: 'userName',
  },
  {
    label: 'Password',
    keySearch: 'passWord',
  },
  {
    label: 'Ngày chấp nhận',
    keySearch: 'startDate',
    inputType: 'DATETIME',
    placeHolder: 'start date'
  },
  {
    label: 'Ngày đóng chuyến thư',
    keySearch: 'endDate',
    inputType: 'DATETIME',
    placeHolder: 'end date'
  },
];

export const arrayOptionSearchUser = [
  {
    label: 'First Name',
    keySearch: 'firstName',
  },
  {
    label: 'Phone',
    keySearch: 'phone',
  },
  {
    label: 'Role',
    keySearch: 'role',
  },
  {
    label: 'Entity Name',
    keySearch: 'asset',
  },
];

export const arrayOptionSearchAsset = [
  {
    label: 'Asset Name',
    keySearch: 'assetName',
  },
  {
    label: 'CEO Name',
    keySearch: 'ceoName',
  },
  {
    label: 'Chairman Name',
    keySearch: 'chairmanName',
  },
  {
    label: 'Cluster',
    keySearch: 'cluster',
  },
];

export const arrayOptionSearchCluster = [
  {
    label: 'Cluster Name',
    keySearch: 'clusterName',
  },
];

export const arrayOptionSearchDocument = [
  {
    label: 'Name',
    keySearch: 'name',
  },
  {
    label: 'Type',
    keySearch: 'type',
  },
  {
    label: 'Month',
    keySearch: 'month',
  },
  {
    label: 'Year',
    keySearch: 'year',
  },
];

export const headCellClusterReport = [
  {
    id: 'stt',
    numeric: true,
    label: 'No',
    applySorting: false,
    disablePadding: true,
    // width: 100,
  },
  {
    id: 'clusterName',
    numeric: false,
    label: 'ClusterName',
    applySorting: true,
    disablePadding: false,
    // width: 100,
  },
  {
    id: 'period',
    numeric: false,
    label: 'Period',
    applySorting: true,
    disablePadding: false,
    // width: 100,
  },
  {
    id: 'createdDate',
    numeric: false,
    label: 'Created Date',
    applySorting: true,
    disablePadding: false,
    // width: 100,
  },

  {
    id: 'createdByUsername',
    numeric: false,
    label: 'Create by',
    applySorting: true,
    disablePadding: false,
    // width: 100,
  },
  {
    id: 'actiom',
    numeric: false,
    label: 'Action',
    applySorting: false,
    disablePadding: false,
    // width: 100,
  },
];

export const formatMonth = (date) => {
  const dateArray = date && date.split('/');
  const newDay = new Date(dateArray[1], dateArray[0], null);
  const month = new Intl.DateTimeFormat('en', { month: 'short', year: 'numeric' }).format(newDay);
  return month;
};

export const formatMonthSelect = (date) => {
  const dateArray = date && date.split('/');
  const newDay = new Date(dateArray[1], dateArray[0], null);
  return newDay;
};

export const formatDateMonthYear = (da) => {
  const monthShortWord = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];
  const date = new Date(da);
  const dd = (date.getDate() < 10 ? '0' : '') + (date.getDate() - 1);
  const mm = (date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1);
  const yy = date.getFullYear();

  return `${dd}-${monthShortWord[mm - 1]}-${yy}`;
};
