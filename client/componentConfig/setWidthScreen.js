const windowSizeByBootstrap = (windowWidth) => {
  
    if(windowWidth >= 1200){
        return  'xl'
    }
    if(windowWidth >= 992){
      
        return 'lg'
    }
    if(windowWidth >= 768){
        return 'md'
    }
    if(windowWidth >= 576){
        return 'sm'
    }
    return 'sm'
}

export const isMobile = (windowWidth) => {
    const size = windowSizeByBootstrap(windowWidth);
    return size === 'sm'
} 

export const isTablet = (windowWidth) => {
    const size = windowSizeByBootstrap(windowWidth);
    return size === 'md'
}

export const isPC = (windowWidth) => {
    const size = windowSizeByBootstrap(windowWidth);
    return ['lg', 'xl'].indexOf(size) !== -1;
}