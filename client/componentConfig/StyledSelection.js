import { InputBase, withStyles } from '@material-ui/core';

export const BootstrapInput = withStyles((theme) => ({
  root: {
    width: '100%',
  },
  input: {
    display: 'flex',
    alignItems: 'center',
    borderRadius: '8px ~important',
    height: '25px',
    position: 'relative',
    border: '1px solid #ced4da',
    fontSize: 16,
    paddingLeft: 14,
    paddingRight: 14,
    transition: theme.transitions.create(['border-color', 'box-shadow']),
  },
}))(InputBase);
