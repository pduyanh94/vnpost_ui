import { cloneElement} from 'react';
import PropTypes from 'prop-types';
import { useScrollTrigger } from '@material-ui/core';

export const ScrollTrigger = (props) => {
    const { children, window } = props;

    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 0,
        target: window ? window() : undefined,
    });

    return cloneElement(children, {
        elevation: trigger ? 4 : 0,
    })
}

ScrollTrigger.propTypes = {
    children: PropTypes.element.isRequired,
    window: PropTypes.func
}