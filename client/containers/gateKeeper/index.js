import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AuthRoutes from '../../routes/authRoutes';
import './style.less';

class GateKeeper extends Component {
  componentDidMount() {
  }

  render() {
    // const { appToken, userDetails } = this.props;
    // const checkAuthen = Object.keys(userDetails).length === 0 ? true : false
    const renderRoutes = <AuthRoutes />
    return renderRoutes;
  }
}

GateKeeper.propTypes = {

};
const mapStateToProps = (state) => ({
  
});
const mapDispatchToProps = (dispatch) => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(GateKeeper);
