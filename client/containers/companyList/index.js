import React, { Fragment, PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import Grid from '@material-ui/core/Grid';
import { Hidden } from '@material-ui/core';
import { connect } from 'react-redux';
import ReactAlert from '../../components/alert';
import { SearchManagement } from '../../components/componentsManagerment/SearchManagerment';
import Header from '../../components/header';
import { getCompanyList } from '../../actions/asyncActions/company_async';
import { arrayOptionSearchCompanyInfo, headCellsDataValidation } from '../../componentConfig';
import EditOutline from '../../assets/icons/edit-outline.svg';
import { AppBarCustom } from '../../componentsMobile/header/AppBarCustom';
import { HeaderManagement } from '../../components/componentsManagerment/HeaderManagement';
import { TableCustom } from '../../components/table/TableCustom';
import { Loading } from '../../components/loading/Loading';

import './style.less';

let user = {};

class CompanyListComponent extends PureComponent {
  state = {
    order: 'asc',
    orderBy: '',
    search: {
      assetName: '',
      month: '',
      year: '',
      status: '',
    },
  };

  handleGetCompanyList = () => {
    const { getCompanyList } = this.props;
    getCompanyList({ search: this.state.search });
  };

  componentDidMount() {
    this.handleGetCompanyList();
  }

  handleRequestSort = (_event, property) => {
    const isAsc = this.state.orderBy === property && this.state.order === 'asc';
    this.setState({
      order: isAsc ? 'desc' : 'asc',
      orderBy: property,
    });
  };

  handleChangeSearch = (search) => {
    this.setState({ search }, () => {
      this.handleGetCompanyList();
    });
  };



  render() {
    const { companyList } = this.props;

    return (
      <Fragment>
        <Helmet>
          <title>Dashboard</title>
        </Helmet>
        <ReactAlert childRef={(ele) => (this.alert = ele)} />
        <Grid container className="adq-company-list">
          <Hidden only={['xs']}>
            <Header />
          </Hidden>

          <Hidden only={['lg', 'md', 'sm', 'xl']}>
            <AppBarCustom titleScreen="Dashboard" />
          </Hidden>

          <Grid item md={12} sm={12} className="company-list-container">
            <Grid container className="table_container">
              <div className="width-100">
                <Hidden only={['xs']}>
                  <HeaderManagement title="Dashboard" />
                </Hidden>
                <SearchManagement
                  fieldInputs={arrayOptionSearchCompanyInfo}
                  onChangeSearch={this.handleChangeSearch}
                  // allDisable={showLoader}
                />
              </div>

              <Grid item md={12} sm={12} className="width-100">

                  <TableCustom
                    rows={companyList}
                    headCells={headCellsDataValidation}
                    roleScreen="data_validation"
                  />
                
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Fragment>
    );
  }
}

const mapStateToprops = (state) => ({
  companyList: state.companyReducer.companyList,
  // showLoader: state.commonReducer.showLoader,
  // message: state.commonReducer.message,
});

const mapDispatchToProps = (dispatch) => ({
  getCompanyList: (params) => dispatch(getCompanyList(params)),
  dispatch,
});

export default connect(mapStateToprops, mapDispatchToProps)(CompanyListComponent);
