const {
  authenticateUser,
  getUserInfo,
  getUserAccessLevelStructure,
  getCompanyList,
  getAllIDUsers,
  postRequest,
  putRequest,
  getRequest,
  deleteRequest,
  patchRequest,
} = require('../services/userService');
const { authUserSchema } = require('../joiSchema');
const { MESSAGES, HTTP_CODE } = require('../constants');
const {
  sendAndLogError,
  webGetApiResponseController,
  webPostApiResponseController,
} = require('../utils');

exports.validateUserCredentials = async (req, res) => {
  try {
    const { body: userDetails, originalUrl } = req;
    logger.info(
      { reqid: req.id, log_type: 'application' },
      `${MESSAGES.logs.userController.userCredForAuth}`,
      userDetails,
    );
    const value = await authUserSchema.validateAsync(userDetails);
    webPostApiResponseController(
      authenticateUser,
      '',
      value,
      originalUrl,
      MESSAGES.api.USER_FIND_ERROR,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.logs.common.validationError);
  }
};

exports.userInfo = async (req, res) => {
  try {
    logger.info({ reqid: req.id, log_type: 'application' }, 'Starting execution of findUserInfo');
    const { headers, originalUrl } = req;
    const token = headers['authorization'];
    webGetApiResponseController(
      getUserInfo,
      token,
      originalUrl,
      MESSAGES.api.USER_NOT_FOUND,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.api.SOMETHING_WENT_WRONG);
  }
};

exports.userAccessLevelStructure = async (req, res) => {
  try {
    logger.info(
      { reqid: req.id, log_type: 'application' },
      'Starting execution of findUserAccessLevelStructure',
    );
    const { headers, originalUrl } = req;
    const token = headers['authorization'];
    webGetApiResponseController(
      getUserAccessLevelStructure,
      token,
      originalUrl,
      MESSAGES.api.ACCESS_LEVEL_NOT_FOUND,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.api.SOMETHING_WENT_WRONG);
  }
};

exports.companyList = async (req, res) => {
  try {
    logger.info({ reqid: req.id, log_type: 'application' }, 'Starting execution of companyList');
    const { headers, originalUrl } = req;
    const token = headers['authorization'];
    webGetApiResponseController(
      getCompanyList,
      token,
      originalUrl,
      MESSAGES.api.ACCESS_LEVEL_NOT_FOUND,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.api.SOMETHING_WENT_WRONG);
  }
};

exports.getAllIDUsers = async (req, res) => {
  try {
    logger.info({ reqid: req.id, log_type: 'application' }, 'Starting execution of getAllIDUsers');
    const { headers, originalUrl } = req;
    const token = headers['authorization'];
    webGetApiResponseController(
      getAllIDUsers,
      token,
      originalUrl,
      MESSAGES.api.ACCESS_LEVEL_NOT_FOUND,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.api.SOMETHING_WENT_WRONG);
  }
};

exports.listUser = async (req, res) => {
  try {
    logger.info({ reqid: req.id, log_type: 'application' }, 'Starting execution of listUser');
    const { headers, originalUrl } = req;
    const token = headers['authorization'];
    webGetApiResponseController(
      getCompanyList,
      token,
      originalUrl,
      MESSAGES.api.ACCESS_LEVEL_NOT_FOUND,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.api.SOMETHING_WENT_WRONG);
  }
};

exports.handleGetRequest = async (req, res) => {
  try {
    const { headers, originalUrl } = req;
    logger.info({ reqid: req.id, log_type: 'application' }, `Starting execution of ${originalUrl}`);
    const token = headers['authorization'];
    webGetApiResponseController(
      getRequest,
      token,
      originalUrl,
      MESSAGES.api.SOMETHING_WENT_WRONG,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.api.SOMETHING_WENT_WRONG);
  }
};

exports.handlePostRequest = async (req, res) => {
  const { headers, body, originalUrl } = req;
  logger.info(
    { reqid: req.id, log_type: 'application' },
    `${MESSAGES.logs.common.postRequest}`,
    body,
  );
  const token = headers['authorization'];
  webPostApiResponseController(
    postRequest,
    token,
    body,
    originalUrl,
    MESSAGES.api.SOMETHING_WENT_WRONG,
    req,
    res,
  );
};

exports.handlePutRequest = async (req, res) => {
  const { headers, body, originalUrl } = req;
  logger.info(
    { reqid: req.id, log_type: 'application' },
    `${MESSAGES.logs.common.putRequest}`,
    body,
  );
  const token = headers['authorization'];
  webPostApiResponseController(
    putRequest,
    token,
    body,
    originalUrl,
    MESSAGES.api.SOMETHING_WENT_WRONG,
    req,
    res,
  );
};

exports.handleDeleteRequest = async (req, res) => {
  const { headers, body, originalUrl } = req;
  logger.info(
    { reqid: req.id, log_type: 'application' },
    `${MESSAGES.logs.common.deleteRequest}`,
    body,
  );
  const token = headers['authorization'];
  webPostApiResponseController(
    deleteRequest,
    token,
    body,
    originalUrl,
    MESSAGES.api.SOMETHING_WENT_WRONG,
    req,
    res,
  );
};

exports.handlePatchRequest = async (req, res) => {
  const { headers, body, originalUrl } = req;
  logger.info(
    { reqid: req.id, log_type: 'application' },
    `${MESSAGES.logs.common.patchRequest}`,
    body,
  );
  const token = headers['authorization'];
  webPostApiResponseController(
    patchRequest,
    token,
    body,
    originalUrl,
    MESSAGES.api.SOMETHING_WENT_WRONG,
    req,
    res,
  );
};
