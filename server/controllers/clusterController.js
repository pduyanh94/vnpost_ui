const {
  authenticateUser,
  getUserInfo,
  getUserAccessLevelStructure,
  getCompanyList,
} = require('../services/userService');
const { authUserSchema } = require('../joiSchema');
const { MESSAGES, HTTP_CODE } = require('../constants');
const {
  sendAndLogError,
  webGetApiResponseController,
  webPostApiResponseController,
} = require('../utils');

exports.validateUserCredentials = async (req, res) => {
  try {
    const { body: userDetails, originalUrl } = req;
    logger.info(
      { reqid: req.id, log_type: 'application' },
      `${MESSAGES.logs.userController.userCredForAuth}`,
      userDetails,
    );
    const value = await authUserSchema.validateAsync(userDetails);
    webPostApiResponseController(
      authenticateUser,
      '',
      value,
      originalUrl,
      MESSAGES.api.USER_FIND_ERROR,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.logs.common.validationError);
  }
};

exports.userInfo = async (req, res) => {
  try {
    logger.info(
      { reqid: req.id, log_type: 'application' },
      'Starting execution of findUserInfo',
    );
    const { headers, originalUrl } = req;
    const token = headers['authorization'];
    webGetApiResponseController(
      getUserInfo,
      token,
      originalUrl,
      MESSAGES.api.USER_NOT_FOUND,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.api.SOMETHING_WENT_WRONG);
  }
};

exports.userAccessLevelStructure = async (req, res) => {
  try {
    logger.info(
      { reqid: req.id, log_type: 'application' },
      'Starting execution of findUserAccessLevelStructure',
    );
    const { headers, originalUrl } = req;
    const token = headers['authorization'];
    webGetApiResponseController(
      getUserAccessLevelStructure,
      token,
      originalUrl,
      MESSAGES.api.ACCESS_LEVEL_NOT_FOUND,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.api.SOMETHING_WENT_WRONG);
  }
};

exports.companyList = async (req, res) => {
  try {
    logger.info(
      { reqid: req.id, log_type: 'application' },
      'Starting execution of companyList',
    );
    const { headers, originalUrl } = req;
    const token = headers['authorization'];
    webGetApiResponseController(
      getCompanyList,
      token,
      originalUrl,
      MESSAGES.api.ACCESS_LEVEL_NOT_FOUND,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.api.SOMETHING_WENT_WRONG);
  }
};
