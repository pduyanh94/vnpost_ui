const {
  getEntityInfo,
  getReportData,
  getKpiDetails,
  addCompanyInformation,
  getCompanyInformationList,
  updateCompanyInformation,
  getSpecificCompanyInformation,
  changeReviewerEntity,
  getEntityAsset,
  getEntityCluster
} = require('../services/entityService');
const {
  entityInfoSchema,
  reportScoreCardSchema,
  kpiDetailsSchema,
  companyInformationSchema,
  specificCompanyInformationSchema,
  updateCompanyInformationStatusSchema,
} = require('../joiSchema');
const { MESSAGES, HTTP_CODE } = require('../constants');
const {
  sendAndLogError,
  webGetApiResponseController,
  webPostApiResponseController,
} = require('../utils');
exports.entityInfo = async (req, res) => {
  try {
    const { query, originalUrl, headers } = req;
    const token = headers.authorization;
    logger.info(
      { reqid: req.id, log_type: 'application' },
      `${MESSAGES.logs.entityController.entityIdForInfo} - ${query}`,
    );
    await entityInfoSchema.validateAsync(query);
    webGetApiResponseController(
      getEntityInfo,
      token,
      originalUrl,
      MESSAGES.api.ENTITY_INFO_NOT_FOUND,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.logs.common.validationError);
  }
};
exports.reportScoreCard = async (req, res) => {
  try {
    const { query, originalUrl, headers } = req;
    const token = headers.authorization;
    const filters = JSON.parse(query.filters);
    logger.info(
      { reqid: req.id, log_type: 'application' },
      `${MESSAGES.logs.entityController.reportDataInfo} - ${filters}`,
    );
    await reportScoreCardSchema.validateAsync(filters);
    webGetApiResponseController(
      getReportData,
      token,
      originalUrl,
      MESSAGES.api.REPORT_NOT_FOUND,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.logs.common.validationError);
  }
};
exports.kpiDetails = async (req, res) => {
  try {
    const { query, originalUrl, headers } = req;
    const token = headers.authorization;
    const filters = JSON.parse(query.filters);
    logger.info(
      { reqid: req.id, log_type: 'application' },
      `${MESSAGES.logs.entityController.kpiDetails} - ${filters}`,
    );
    await kpiDetailsSchema.validateAsync(filters);
    webGetApiResponseController(
      getKpiDetails,
      token,
      originalUrl,
      MESSAGES.api.KPI_NOT_FOUND,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.logs.common.validationError);
  }
};
exports.addUpdateCompanyInformation = async (req, res) => {
  try {
    const { headers, originalUrl, body } = req;
    const logMsg = originalUrl.includes('update')
      ? 'updateCompanyInformation'
      : 'addCompanyInformation';
    const token = headers.authorization;
    logger.info(
      { reqid: req.id, log_type: 'application' },
      `${MESSAGES.logs.entityController[logMsg]}`,
      body,
    );
    const value = await companyInformationSchema.validateAsync(body);
    originalUrl.includes('addCompanyInformation')
      ? webPostApiResponseController(
          addCompanyInformation,
          token,
          value,
          originalUrl,
          '',
          req,
          res,
        )
      : webPostApiResponseController(
          updateCompanyInformation,
          token,
          value,
          originalUrl,
          '',
          req,
          res,
        );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.logs.common.validationError);
  }
};
exports.companyInformation = async (req, res) => {
  try {
    const { headers, originalUrl, query } = req;
    const token = headers.authorization;
    if (originalUrl.includes('specificCompanyInformation')) {
      logger.info(
        { reqid: req.id, log_type: 'application' },
        `${MESSAGES.logs.entityController.companyInformationFor} - ${query}`,
      );
      await specificCompanyInformationSchema.validateAsync(query);
      webGetApiResponseController(
        getSpecificCompanyInformation,
        token,
        originalUrl,
        MESSAGES.api.NO_RESOURCE_FOUND,
        req,
        res,
      );
    } else {
      webGetApiResponseController(
        getCompanyInformationList,
        token,
        originalUrl,
        MESSAGES.api.NO_RESOURCE_FOUND,
        req,
        res,
      );
    }
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.logs.common.notFoundError);
  }
};
exports.updateCompanyInformationStatus = async (req, res) => {
  try {
    const { headers, originalUrl, body } = req;
    const token = headers.authorization;
    logger.info(
      { reqid: req.id, log_type: 'application' },
      `${MESSAGES.logs.entityController.updateCompanyInformation}`,
      body,
    );
    const value = await updateCompanyInformationStatusSchema.validateAsync(
      body,
    );
    webPostApiResponseController(
      updateCompanyInformation,
      token,
      value,
      originalUrl,
      '',
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.logs.common.validationError);
  }
};
exports.changeReviewerEntity = async (req, res) => {
  try {
    const { body, originalUrl } = req;
    logger.info(
      { reqid: req.id, log_type: 'application' },
      `${MESSAGES.logs.userController.userCredForAuth}`,
      body,
    );
    webPostApiResponseController(
      changeReviewerEntity,
      '',
      body,
      originalUrl,
      MESSAGES.api.USER_FIND_ERROR,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.logs.common.validationError);
  }
};
exports.getEntityAsset = async (req, res) => {
  try {
    logger.info(
      { reqid: req.id, log_type: 'application' },
      'Starting execution of getEntityAsset',
    );
    const { headers, originalUrl } = req;
    const token = headers['authorization'];
    webGetApiResponseController(
      getEntityAsset,
      token,
      originalUrl,
      MESSAGES.api.ACCESS_LEVEL_NOT_FOUND,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.api.SOMETHING_WENT_WRONG);
  }
};
exports.getEntityCluster = async (req, res) => {
  try {
    logger.info(
      { reqid: req.id, log_type: 'application' },
      'Starting execution of getEntityCluster',
    );
    const { headers, originalUrl } = req;
    const token = headers['authorization'];
    webGetApiResponseController(
      getEntityCluster,
      token,
      originalUrl,
      MESSAGES.api.ACCESS_LEVEL_NOT_FOUND,
      req,
      res,
    );
  } catch (error) {
    sendAndLogError(res, error, req, MESSAGES.api.SOMETHING_WENT_WRONG);
  }
};