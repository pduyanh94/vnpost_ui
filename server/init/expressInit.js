const express = require('express');
const helmet = require('helmet');
const compression = require('compression');
const { apiResponseGenerator } = require('./initialiseResponseUtils');
const webpack = require('webpack');

module.exports = (app) => {

  app.set('port', process.env.PORT || 3000);
  if (
    process.env.NODE_ENV === 'production' ||
    process.env.NODE_ENV === 'staging'
  ) {
    // Secure your Express apps by setting various HTTP headers
    app.use(helmet());
    app.use(compression());
  }
  app.use(apiResponseGenerator);
  app.use(express.json());

  if (process.env.NODE_ENV === 'development') {
    
    const webpack = require('webpack');
    const webpackDevMiddleware = require('webpack-dev-middleware');
    const webpackHotMiddleware = require('webpack-hot-middleware');
    const webpackConfig = require('../../webpack/webpack.config');
    const devWebpackConfig = webpackConfig();
    const compiler = webpack(devWebpackConfig);
    app.use(
      webpackDevMiddleware(compiler, {
        noInfo: true,
        stats: 'errors-only',
        publicPath: devWebpackConfig.output.publicPath,
      }),
    );
    app.use(webpackHotMiddleware(compiler));
  }

  logger.info(
    `===> Starting Server . . . ===>  Environment: ${
      process.env.NODE_ENV
    } ===>  Listening on port: ${app.get('port')}`,
  );
};
