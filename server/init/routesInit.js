const cors = require('cors');

const user = require('../routes/userRoutes');
const entity = require('../routes/entityRoutes');
const cluster = require('../routes/clusterRoutes');
const scorecard = require('../routes/scorecardRoutes');
const commentTask = require('../routes/commentTask');

module.exports = (app) => {
  //to enable cross-origin policy
  app.options('*', cors());

  //to add reqId in req object
  app.use((req, res, next) => {
    req.id = Math.random();
    next();
  });

  //user routes
  app.use(`/api/v1/user`, user);

  //entity routes
  app.use(`/api/v1/entity`, entity);
  app.use(`/api/v1/cluster`, cluster);
  app.use(`/api/v1/scoreCard`, scorecard);
  app.use(`/api/v1/commentTask`, commentTask);
};
