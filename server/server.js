require('dotenv').config();
const express = require('express');
const initExpress = require('./init/expressInit');
const initRoutes = require('./init/routesInit');
const path = require('path');
global.logger = require('./configurations/logger');

const { createCommentTask, getCommentTaskList } = require('./services/commentTaskService');

const app = express();


/*
 * serving static files or assets
 */

app.use(express.static('./build'));

/*
 * express settings
 */

initExpress(app);

/*
 * server application routes
 *
 */

initRoutes(app);

/*
 * to serve favicon icon
 */

app.get('/favicon', (req, res) => {
  res.sendFile(path.resolve('favicon.ico'));
});

/*
 * For any routes return index.html
 */

app.get('/*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../build/index.html'));
});

// app.listen(app.get('port'));
const server = app.listen(app.get('port'));
let io = require('socket.io')(server);

const _ = require('lodash');
const notificationNameSpace = io.of('/notifications');
// const { authenticateSocketIO, checkAuthenticateSocketIO } = require("./utils/index");

notificationNameSpace.on('connection', async (socket) => {
  console.log('Socket: Client connected');
  socket.on('client.emit.authenticate', ({ authData, userId }) => {
    //   const authenticateResult = authenticateSocketIO(authData || {});
    socket.userId = userId;
    socket.join(userId);
    //   if (true || authenticateResult.status) {
    //       socket.auth = true;
    //       socket.authData = authenticateResult.data
    //   }
  });

  socket.on('client.emit.getCommentTaskList', (filter) => {
    // checkAuthenticateSocketIO(socket);
    getCommentTaskList(socket.userId, filter)
      .then((res) => {
        notificationNameSpace
          .to(socket.userId)
          .emit('server.emit.getCommentTaskList', res.data.data);
      })
      .catch((error) => {
        return error;
      });
  });

  socket.on('client.emit.createCommentTask', (commentTaskDetail) => {
    // checkAuthenticateSocketIO(socket);
    createCommentTask(commentTaskDetail)
      .then((res) => {
        notificationNameSpace.to(commentTaskDetail.reviewerId).emit('server.emit.update');
      })
      .catch((error) => {
        return error;
      });
  });
});
