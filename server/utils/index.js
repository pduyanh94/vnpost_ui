const axios = require('axios');
const { HTTP_CODE, MESSAGES } = require('../constants');
/**
 * @param {req} req object
 * @param {res} res object
 * @param {next} next function of this middleware
 */
exports.isAuthenticated = (req, res, next) => {
  const token = req.headers.authorization;
  // if (!token) {
  //   logger.error(
  //     { reqid: req.id, log_type: 'application' },
  //     MESSAGES.api.AUTH_ERROR,
  //   );
  //   this.logResponse(req, HTTP_CODE.UNAUTHORIZED);
  //   return res.sendError(HTTP_CODE.UNAUTHORIZED, MESSAGES.api.AUTH_ERROR);
  // }
  next();
};
/**
 * @param {res} res object
 * @param {error} error object
 * @param {validationMessage} validationMessage string
 */
exports.sendAndLogError = (res, error, req, message) => {
  const { details } = error;
  logger.error({ reqid: req.id, error, log_type: 'application' }, message);
  const validationError = details && details.length ? details[0].message : [];
  if (validationError.length) {
    this.logResponse(req, HTTP_CODE.VALIDATION_ERROR);
    res.sendValidationError({ validationError });
  } else {
    this.logResponse(req, HTTP_CODE.UN_PROCESSABLE_ENTITY);
    res.sendError(HTTP_CODE.UN_PROCESSABLE_ENTITY, MESSAGES.api.SOMETHING_WENT_WRONG);
  }
};
/**
 * @param {accessToken} accessToken string
 */
const getConfig = (accessToken) => {
  const config = {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: accessToken,
    },
    timeout: 60000
  };
  return {
    config,
  };
};
/**
 * @param {accessToken} accessToken string
 * @param {url} url string
 */
exports.webApiGet = (accessToken, url) => {
  const config = getConfig(accessToken);
  return {
    request: axios.get(url, config.config),
  };
};
/**
 * @param {accessToken} accessToken string
 * @param {url} url string
 * @param {options} options object
 */
exports.webApiPost = (accessToken, url, options) => {
  const config = getConfig(accessToken);
  return {
    request: axios.post(url, options, config.config),
  };
};
exports.webApiPatch = (accessToken, url, options) => {
  const config = getConfig(accessToken);
  return {
    request: axios.patch(url, options, config.config),
  };
};

exports.webApiPut = (accessToken, url, options) => {
  const config = getConfig(accessToken);
  return {
    request: axios.put(url, options, config.config),
  };
};

exports.webApiDelete = (accessToken, url, options) => {
  const config = getConfig(accessToken);
  return {
    request: axios.delete(url, options, config.config),
  };
};

/**
 * @param {controllerFunction} controllerFunction function
 * @param {token} token string
 * @param {reqUrl} reqUrl string
 * @param {message} message string
 * @param {res} res object
 */
exports.webGetApiResponseController = (controllerFunction, token, reqUrl, message, req, res) => {
  controllerFunction(token, reqUrl)
    .then((response) => {
      if (response.data) {
        const {
          status,
          data: { data: responseData },
        } = response;
        this.logResponse(req, HTTP_CODE.SUCCESS);
        res.sendSuccess(status, responseData);
      } else {
        this.logResponse(req, HTTP_CODE.NOT_FOUND);
        res.sendError(HTTP_CODE.NOT_FOUND, message);
      }
    })
    .catch((error) => errorResponseHandler(error, req, res));
};
/**
 * @param {controllerFunction} controllerFunction function
 * @param {token} token string
 * @param {data} data object/string
 * @param {reqUrl} reqUrl string
 * @param {message} message string
 * @param {res} res object
 */
exports.webPostApiResponseController = (
  controllerFunction,
  token,
  data,
  reqUrl,
  message,
  req,
  res,
) => {
  controllerFunction(token, data, reqUrl)
    .then((response) => {
      if (response.data) {
        const {
          status,
          data: { data: responseData },
        } = response;
        this.logResponse(req, HTTP_CODE.SUCCESS);
        res.sendSuccess(status, responseData);
      } else {
        this.logResponse(req, HTTP_CODE.UN_PROCESSABLE_ENTITY);
        res.sendError(HTTP_CODE.UN_PROCESSABLE_ENTITY);
      }
    })
    .catch((error) => errorResponseHandler(error, req, res));
};
/**
 * @param {error} error object
 * @param {res} res object
 */
const errorResponseHandler = (error, req, res) => {
  if (error.response && error.response.data) {
    logger.error({ reqid: req.id, error, log_type: 'application' });
    const {
      status,
      data: {
        error: { message, data },
      },
    } = error.response;
    this.logResponse(req, status);
    res.sendError(status, message, data);
  } else this.sendAndLogError(res, error, req, MESSAGES.api.SOMETHING_WENT_WRONG);
};
exports.logResponse = (req, statusCode) => {
  const log = logger.child({
    reqid: req.id,
    res: {
      statusCode: statusCode,
    },
    log_type: 'access',
  });
  log.info({ req: req }, 'finish');
};
