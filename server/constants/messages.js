module.exports = {
  api: {
    SUCCESS: 'Success result',
    USER_FIND_ERROR: 'Something went wrong while finding the data',
    USER_NOT_FOUND: 'User not found',
    NO_RESOURCE_FOUND: 'Requested resource not found.',
    ENTITY_INFO_NOT_FOUND: 'Entity information not found',
    EMAIL_VERIFIED: 'Your Account is Verified successfully.',
    SOMETHING_WENT_WRONG: 'Sorry !! Something went wrong. Please try after some time.',
    SERVER_ERROR: 'Error occurred on server. Please, report it back to team.',
    AUTH_ERROR: 'Failed to authenticate token.',
    INVALID_LOGIN: 'Login information is not valid',
    INVALID_ROLE: "You can't access this service with this role",
    APPLICATION_TOKEN_DELETE: 'Token has been removed successfully.',
    ACCESS_LEVEL_NOT_FOUND: 'User access level information not found',
    REPORT_NOT_FOUND: 'Report data not found',
    KPI_NOT_FOUND: 'Kpi not found',
  },
  validations: {
    VALIDATION_ERROR: 'validation error',
    INVALID_EMAIL: 'Email is not valid',
    INVALID_PAYLOAD: 'Unable to process the request.',
    INVALID_ROLE: 'Invalid Role',
    PASSWORD_REQUIRED: 'Password is required for login',
  },
  logs: {
    common: {
      validationError: 'Error occured while validating the requested resource',
      notFoundError: 'Error occured while fetching the requested resource',
      postRequest: 'Handle post request',
      putRequest: 'Handle put request',
      deleteRequest: 'Handle delete request',
      patchRequest: 'Handle patch request',
    },
    userController: {
      userCredForAuth: 'User cred for authentication',
    },
    entityController: {
      entityIdForInfo: 'Get entity info for',
      reportDataInfo: 'Get report data for',
      kpiDetails: 'Get kpi details for',
      addCompanyInformation: 'Company details for insertion',
      updateCompanyInformation: 'Update company details',
      contentInsertionDetails: 'Content details for insertion',
      companyInformationFor: 'Get companyInformation for',
    },
  },
};
