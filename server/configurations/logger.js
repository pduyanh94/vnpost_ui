var bunyan = require('bunyan');

const logger = bunyan.createLogger({
  name: process.env.APP_NAME || 'ReportingApplicationUI',
  serializers: {
    req: bunyan.stdSerializers.req,
    res: bunyan.stdSerializers.res,
  },
  src: true,
});

module.exports = logger;
