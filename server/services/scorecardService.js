const { webApiGet, webApiPost } = require('../utils');

const base = `https://${process.env.API_HOST}`;
//const base = `https://adminapi-dev-adqbi.adq.ae`

exports.authenticateUser = (token, data, reqUrl) => {
  const url = `${base}${reqUrl}`;
  console.log('login', url);
  return webApiPost('', url, data).request;
};

exports.getUserInfo = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};

exports.getUserAccessLevelStructure = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};

exports.getCompanyList = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};
