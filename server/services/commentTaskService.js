const { webApiGet, webApiPost, webApiPut } = require('../utils');

const base = `https://${process.env.API_HOST}`;
//const base = `https://adminapi-dev-adqbi.adq.ae`

exports.createCommentTask = (data) => {
  const url = `${base}/api/v1/commentTask`;
  return webApiPost('', url, data).request;
};

exports.getCommentTaskList = (userId, { filter }) => {
  const { order, pagination } = filter;
  const url = `${base}/api/v1/commentTask?filter={"order":{"orderBy":"${order.orderBy}","direction":${order.direction}}, "search": {"reviewerId": ${userId}}, "pagination":{"currentPage":${pagination.currentPage},"limit":${pagination.limit}}}`;
  return webApiGet('', url).request;
};
