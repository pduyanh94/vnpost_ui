const { webApiGet, webApiPost, webApiPatch } = require('../utils');

const base = `http://10.35.104.142:3000`;
//const base = `https://adminapi-dev-adqbi.adq.ae`

exports.getEntityInfo = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(url).request;
};
exports.getReportData = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};
exports.getKpiDetails = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};
exports.addCompanyInformation = (token, data, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiPost(token, url, data).request;
};
exports.updateCompanyInformation = (token, data, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiPatch(token, url, data).request;
};
exports.getCompanyInformationList = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};
exports.getSpecificCompanyInformation = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};
exports.changeReviewerEntity = (token, data, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiPost(token, url, data).request;
};

exports.getEntityAsset = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};

exports.getEntityCluster = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};
