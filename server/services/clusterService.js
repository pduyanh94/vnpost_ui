const { webApiGet, webApiPost } = require('../utils');

const base = `http://${process.env.API_HOST}:3000`;
//const base = `https://adminapi-dev-adqbi.adq.ae`

exports.authenticateUser = (token, data, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiPost('', url, data).request;
};

exports.getUserInfo = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};

exports.getUserAccessLevelStructure = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};

exports.getCompanyList = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};
