const { webApiGet, webApiPost, webApiPut, webApiDelete, webApiPatch } = require('../utils');

const base = `https://${process.env.API_HOST}`;
//const base = `https://adminapi-dev-adqbi.adq.ae`

exports.authenticateUser = (token, data, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiPost('', url, data).request;
};

exports.getUserInfo = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};

exports.getUserAccessLevelStructure = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};

exports.getCompanyList = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};

exports.getAllIDUsers = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};

exports.getRequest = (token, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiGet(token, url).request;
};

exports.postRequest = (token, data, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiPost(token, url, data).request;
};

exports.putRequest = (token, data, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiPut(token, url, data).request;
};

exports.deleteRequest = (token, data, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiDelete(token, url, data).request;
};

exports.patchRequest = (token, data, reqUrl) => {
  const url = `${base}${reqUrl}`;
  return webApiPatch(token, url, data).request;
};
