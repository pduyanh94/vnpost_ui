const Joi = require('@hapi/joi');

exports.authUserSchema = Joi.object({
  username: Joi.string().invalid('', null).required(),
  password: Joi.string().invalid('', null).required(),
});

exports.entityInfoSchema = Joi.object({
  entityId: Joi.number().required(),
});

exports.reportScoreCardSchema = Joi.object({
  entityId: Joi.number().required().prefs({ convert: false }),
  month: Joi.number().required().prefs({ convert: false }),
  year: Joi.number().required().prefs({ convert: false }),
  reportName: Joi.string().invalid('', null).required(),
});

exports.kpiDetailsSchema = Joi.object({
  entityId: Joi.number().required().prefs({ convert: false }),
  year: Joi.number().required().prefs({ convert: false }),
  kpiId: Joi.number().required().prefs({ convert: false }),
});
exports.companyInformationSchema = Joi.object({
  id: Joi.number().required().prefs({ convert: false }),
  kbdLeftHeader: Joi.string().invalid('', null).required(),
  kbdLeftContent: Joi.string().invalid('', null).required(),
  kbdRightHeader: Joi.string().invalid('', null).required(),
  kbdRightContent: Joi.string().invalid('', null).required(),
  fpiRightContent: Joi.string().invalid('', null).required(),
  fpiLeftContent: Joi.string().invalid('', null).required(),
  fpiRightHeader: Joi.string().invalid('', null).required(),
  fpiLeftHeader: Joi.string().invalid('', null).required(),
  owLeftHeader: Joi.string().invalid('', null).required(),
  owLeftContent: Joi.string().invalid('', null).required(),
  owRightHeader: Joi.string().invalid('', null).required(),
  owRightContent: Joi.string().invalid('', null).required(),
});
exports.specificCompanyInformationSchema = Joi.object({
  id: Joi.number().required(),
});
exports.updateCompanyInformationStatusSchema = Joi.object({
  id: Joi.number().required().prefs({ convert: false }),
  status: Joi.string().invalid('', null).required(),
  rejectedComments: Joi.string().allow('', null),
  approvedComments: Joi.string().allow('', null),
  overturnedComments: Joi.string().allow('', null),
});
