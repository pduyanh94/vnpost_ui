const express = require('express');

const { isAuthenticated } = require('../utils');
const {
  validateUserCredentials,
  userInfo,
  userAccessLevelStructure,
  companyList,
  getAllIDUsers,
  listUser,
  handlePostRequest,
  handlePutRequest
} = require('../controllers/userController');

const route = express.Router();

route.post('/login', validateUserCredentials);
route.get('/info', isAuthenticated, userInfo);
route.get('/accessLevelStructure', isAuthenticated, userAccessLevelStructure);
route.get('/companyList', isAuthenticated, companyList);
route.get('/getAllIDUsers', isAuthenticated, getAllIDUsers)
route.get('/listUser', isAuthenticated, listUser)
route.post('/addUser', isAuthenticated, handlePostRequest)
route.put('/editUser/:userId', isAuthenticated, handlePutRequest)
route.post('/deleteUser', isAuthenticated, handlePostRequest)
route.post('/status', isAuthenticated, handlePostRequest)


module.exports = route;
