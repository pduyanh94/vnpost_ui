const express = require('express');
const sql = require('mssql');

const { isAuthenticated } = require('../utils');
const {
  entityInfo,
  reportScoreCard,
  kpiDetails,
  addUpdateCompanyInformation,
  companyInformation,
  updateCompanyInformationStatus,
  changeReviewerEntity,
  getEntityAsset,
  getEntityCluster,
} = require('../controllers/entityController');
const {
  handleGetRequest,
  handlePostRequest,
  handlePutRequest,
  handleDeleteRequest,
  handlePatchRequest,
} = require('../controllers/userController');

const dbConfig = {
  user: 'sa',
  password: '123456',
  database: "431300",
  server: "127.0.0.1",
  port: 1433,
  // driver: "msnodesqlv8",
  // options: {
  //   trustedConnection: true
  // }
  options: {
    cryptoCredentialsDetails: {
      minVersion: 'TLSv1'
    },
    trustServerCertificate: true,
  }
};

const handleGetAssetCompanyList = async (req, res) => {
  try {
    const {
      filter
    } = req.query;
    const filter1 = JSON.parse(filter);
    const {
      search: {
        databaseURL,
        startDate,
        endDate,
        userName,
        databaseName,
        passWord
      }
    } = filter1;
    const date1 = new Date(startDate);
    const date2 = new Date(endDate);
    const firstDate = `${date1.getFullYear()}${date1.getMonth() + 1 > 10 ? date1.getMonth() + 1 : `0${date1.getMonth() + 1}`}${date1.getDate() > 10 ? date1.getDate() : `0${date1.getDate()}`}`;
    const secondDate = `${date2.getFullYear()}${date2.getMonth() + 1 > 10 ? date2.getMonth() + 1 : `0${date2.getMonth() + 1}`}${date2.getDate() > 10 ? date2.getDate() : `0${date2.getDate()}`}`;
    console.log(firstDate, secondDate, '.........');
    dbConfig.database = databaseName;
    dbConfig.password = passWord;
    dbConfig.user = userName;
    dbConfig.server = databaseURL;
    const query =
      `select Itemcode, CustomerCode from ITem  where convert(date,sendingTime,103)='${firstDate}'
    and ItemCode not in (select Itemcode from Dispatch
    where Year>='${secondDate}')`;
    await sql.connect(dbConfig);
    const result = await sql.query(query);
    const arrayResult = result ? result.recordset : [];
    console.log(arrayResult);
    res.json({
      data: arrayResult,
      status: 200,
    });
  } catch (error) {
    res.status(500);
    res.json(error);
  }
};

const route = express.Router();

route.get('/assetCompanyList', handleGetAssetCompanyList);


module.exports = route;
