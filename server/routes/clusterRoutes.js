const express = require('express');

const { isAuthenticated } = require('../utils');
const {
  handleGetRequest,
  handlePostRequest,
  handlePutRequest,
  handleDeleteRequest,
} = require('../controllers/userController');

const route = express.Router();

route.get('/', isAuthenticated, handleGetRequest);
route.post('/', isAuthenticated, handlePostRequest);
route.put('/:clusterId', isAuthenticated, handlePutRequest);
route.delete('/:clusterId', isAuthenticated, handleDeleteRequest);

module.exports = route;
