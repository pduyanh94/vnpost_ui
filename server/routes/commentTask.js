const express = require('express');

const { isAuthenticated } = require('../utils');
const {
  handlePutRequest,
} = require('../controllers/userController');

const route = express.Router();

route.put('/:id', isAuthenticated, handlePutRequest);

module.exports = route;
