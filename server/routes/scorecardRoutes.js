const express = require('express');

const { isAuthenticated } = require('../utils');
const {
  handleGetRequest,
  handlePostRequest,
  handlePutRequest,
  handleDeleteRequest,
} = require('../controllers/userController');

const route = express.Router();

route.get('/', isAuthenticated, handleGetRequest);
route.get('/masterEntity', isAuthenticated, handleGetRequest);
route.post('/', isAuthenticated, handlePostRequest);
route.get('/:scorecardId', isAuthenticated, handleGetRequest);
route.put('/:scorecardId', isAuthenticated, handlePutRequest);
route.delete('/:scorecardId', isAuthenticated, handleDeleteRequest);
route.post('/scoreCardKpi/:scorecardId', isAuthenticated, handlePostRequest);
route.post('/saveAsDraft/:scorecardId', isAuthenticated, handlePostRequest);

module.exports = route;
